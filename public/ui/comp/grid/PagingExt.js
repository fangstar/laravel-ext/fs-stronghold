/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

Ext.define('app.comp.grid.PagingExt', {
    extend: 'Ext.toolbar.Paging',

    alias: 'widget.pagingtoolbarExt',

    preventItemsBubble: true,
    showMoveRow       : false,
    prevRowText       : '上一条',
    nextRowText       : '下一条',

    pageSizes     : [50, 100, 200, 500],
    extContent    : [],
    getPagingItems: function () {
        var me = this;
        var items = me.callParent();
        debugger
        items.unshift({xtype: 'tbseparator', hidden: !me.showMoveRow});
        items.unshift({itemId: 'nextRow', tooltip: me.nextRowText, overflowText: me.nextRowText, text: me.nextRowText, iconCls: Ext.baseCSSPrefix + 'tbar-row-next', disabled: true, hidden: !me.showMoveRow, handler: me.moveNextRow, scope: me});
        items.unshift({itemId: 'prevRow', tooltip: me.prevRowText, overflowText: me.prevRowText, text: me.prevRowText, iconCls: Ext.baseCSSPrefix + 'tbar-row-prev', disabled: true, hidden: !me.showMoveRow, handler: me.movePrevRow, scope: me});
        items = items.concat(['-', '每页', {
            xtype      : 'combobox',
            itemId     : 'pageLengthItem',
            name       : 'pageLengthItem',
            submitValue: false,
            width      : 60,
            isFormField: false,
            store      : me.pageSizes,
            value      : me.store.pageSize,
            editable   : false,
            listeners  : {
                change: function (combo, newValue, oldValue, eOpts) {
                    me.store.setPageSize(newValue);
                    me.moveFirst();
                }
            }
        }, '条']);
        if (me.extContent && Ext.isArray(me.extContent) && me.extContent.length > 0) {
            items = items.concat(me.extContent);
        }
        return items;
    },

    initComponent: function () {
        var me = this;
        me.callParent();
        me.bindRowEvent();
    },


    bindRowEvent: function () {
        var me   = this,
            grid = me.up('grid');

        if (!grid) {
            return;
        }
        grid.on('select', function (grid, rec, index, eOpts) {
            if (grid.getSelection().length > 1) {
                me.setChildDisabled('#prevRow', true);
                me.setChildDisabled('#nextRow', true);
            } else {
                me.setChildDisabled('#prevRow', index === 0);
                me.setChildDisabled('#nextRow', index === grid.store.getCount() - 1);
            }
        }, me);
    },

    // @private
    beforeLoad: function () {
        var me = this;
        me.callParent();
        me.setChildDisabled('#prevRow', true);
        me.setChildDisabled('#nextRow', true);
    },

    _moveRow: function (inc) {
        var me        = this,
            grid      = me.up('gridpanel'),
            view      = grid.getView(),
            store     = grid.store,
            sel       = grid.getSelectionModel(),
            selection = sel.getSelection(),
            index;

        if (selection.length != 1) {
            return;
        }

        index = store.indexOf(selection[0]) + inc;

        if (index < 0 || index >= store.getCount()) {
            return;
        }

        sel.select(index);
        view.focusRow(index);
    },

    movePrevRow: function () {
        this._moveRow(-1);
    },

    moveNextRow: function () {
        this._moveRow(1);
    }

});
