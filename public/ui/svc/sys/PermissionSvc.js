Ext.define('app.svc.sys.PermissionSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        getMenus          : function () {
            return app.svc.BaseSvc.ajax.call(this, {url: 'fs/permission/menus/manage'});
        },
        roleListStore     : function () {
            return Ext.create('app.data.store.AbstractStore', {
                url: '/fs/permission/role/list'
            })
        },
        roleAdd           : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: 'fs/permission/role/add', params: params, method: 'POST'});
        },
        roleUpdate        : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: 'fs/permission/role/update', params: params, method: 'POST'});
        },
        roleDel           : function (role_id) {
            return app.svc.BaseSvc.ajax.call(this, {url: 'fs/permission/role/del', params: {sys_role_id: role_id}, method: 'POST'});
        },
        rolePermission    : function (role_id) {
            return app.svc.BaseSvc.ajax.call(this, {url: 'fs/permission/role/permission', mask: [this], params: {sys_role_id: role_id}});
        },
        saveRolePermission: function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: 'fs/permission/role/save-permission', jsonData: params, method: 'POST'});
        }
    }
})