Ext.define('app.viewport.desktop.DesktopStartMenu', {
    extend      : 'Ext.ux.desktop.Module',
    requires    : ['app.comp.Topic', 'app.constant.TopicEvent'],
    node        : {},
    init        : function () {
        var me = this;
        var node = me.node;
        me.launcher = {
            text   : node.node_name,
            iconCls: node.node_icon_cls,
            handler: function () {
                return false;
            },
            menu   : {
                items: []
            }
        }
        if (node.items && Ext.isArray(node.items)) {
            for (var i = 0; i < node.items.length; i++) {
                var cnode = node.items[i];
                this.launcher.menu.items.push({
                                                  text    : cnode.node_name,
                                                  iconCls : cnode.node_icon_cls,
                                                  handler : this.createWindow,
                                                  scope   : this,
                                                  module  : cnode.node_route_path,
                                                  windowId: 'fs_window_' + cnode.node_id,
                                                  node    : cnode
                                              });
            }
        }
    },
    createWindow: function (menu) {
        var me = this
        var node = menu.node;
        if (!node.node_route_path) {
            return null;
        }
        var desktop = me.app.getDesktop();
        app.comp.Topic.publish(app.constant.TopicEvent.SYS.APP_DESKTOP_CREATE_WIN, {desktop: desktop, node: node});
    }
})