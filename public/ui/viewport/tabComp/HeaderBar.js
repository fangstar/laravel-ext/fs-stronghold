Ext.define('app.viewport.tabComp.HeaderBar', {
    extend       : 'Ext.container.Container',
    alias        : ['widget.tabheaderbar'],
    height       : 40,
    requires     : ['app.comp.AuthComp'],
    layout       : {type: 'hbox', align: 'middle'},
    padding      : '0 20px',
    style        : {backgroundColor: '#FFFFFF'},
    initComponent: function () {
        var me = this;
        var emp = app.comp.AuthComp.getEmpInfo.call(me);
        me.items = [
            {
                xtype : 'image',
                src   : '/img/logo_text.png',
                height: 26
            },
            {
                xtype: 'component',
                flex : 1,
                align: 'end',
                tpl  : new Ext.XTemplate(
                    '<div style="text-align:right;cursor:pointer"><span style="margin-right: 10px;">您好，{dept_name} {emp_name}({emp_no})</span><span><a class="opt-icon iconfont sh-topbar-quit opt-btn" opt-type="logout">退出</a></span></div>'
                ),
                data : {
                    emp_name : emp.emp_name,
                    emp_no   : emp.emp_no,
                    dept_name: emp.dept_name
                }
            }
        ];
        me.callParent(arguments);
    },
    afterRender  : function () {
        var me = this;
        me.callParent(arguments);
        var elArry = Ext.query('[opt-type=logout]', false, me.el)
        if (elArry.length > 0) {
            elArry[0].on('click', me.logoutClick, me)
        }
    },
    logoutClick  : function () {
        app.comp.AuthComp.logout();
    }
})