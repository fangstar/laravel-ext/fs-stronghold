Ext.define('app.constant.Sys', {
    statics: {
        conf      : {
            user: {
                desktopShortcut: 'user_desktop_shortcut'
            }
        },
        rootNodeId: 0,
        errCode   : {
            NO_LOGIN: 100
        },
        role      : {
            cateSys : 1,
            cateUser: 2
        }
    }
})