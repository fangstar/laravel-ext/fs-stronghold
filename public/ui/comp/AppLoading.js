Ext.define('app.comp.AppLoading', function () {
    var myMsg = Ext.create('Ext.window.MessageBox', {
        header     : false,
        border     : false,
        bodyBorder : false,
        style      : 'border:0px',
        closeAction: 'destroy',
        modal      : false,
        shadow     : false,
        html       : '<div><img width="250px" height="110px" src="/img/loading.gif" /></div>'
    })
    return {
        statics  : {
            show: function () {
                myMsg.show({})
            },
            hide: function () {
                myMsg.hide()
            }
        }
    }
})