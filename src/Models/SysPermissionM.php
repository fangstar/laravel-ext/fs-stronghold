<?php

namespace Fstar\Stronghold\Models;

class SysPermissionM extends BaseModel {
    protected $table = 'sys_permission';
    protected $primaryKey = 'sys_permission_id';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;
}