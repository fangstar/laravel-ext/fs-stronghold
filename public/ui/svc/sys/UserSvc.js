Ext.define('app.svc.sys.UserSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        getUserConf: function () {
            return app.svc.BaseSvc.ajax.call(this, {url: '/fs/user/get-conf'});
        }
    }
})