<?php

namespace Fstar\Stronghold\Models;

class SysPermissionGroupM extends BaseModel {
    protected $table = 'sys_permission_group';
    protected $primaryKey = 'sys_permission_group_id';
}