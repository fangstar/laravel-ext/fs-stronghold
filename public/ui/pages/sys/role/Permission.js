/**
 * 分配角色权限.
 */
Ext.define('app.pages.sys.role.Permission', {
    extend              : 'Ext.window.Window',
    requires            : ['app.util.Msg', 'app.svc.sys.PermissionSvc'],
    mixins              : ['app.comp.mixin.CompDeferred'],
    width               : 1000,
    height              : 600,
    minWidth            : 800,
    minHeight           : 560,
    title               : '角色分配权限',
    maximizable         : true,
    rowItemCnt          : 4,
    modal               : true,
    $record             : null,
    layout              : 'border',
    _permissionGroupCard: {},
    initComponent       : function () {
        var me = this;
        me.items = [
            {
                xtype       : 'treepanel',
                region      : 'west',
                width       : 200,
                rootVisible : false,
                split       : true,
                displayField: 'permission_group_name',
                listeners   : {
                    itemclick: me._onGroupItemClick,
                    scope    : me
                }
            },
            {
                xtype : 'form',
                region: 'center',
                layout: 'card'
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._save, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ];
        me.callParent(arguments);
    },
    afterRender         : function () {
        var me = this;
        me.callParent(arguments);
        if (!me.$record) {
            app.util.Msg.error('缺少角色记录');
            me.destroy();
            return;
        }
        me._initData()
    },
    _initData           : function () {
        let me = this
        app.svc.sys.PermissionSvc.rolePermission.call(me, me.$record.get('sys_role_id')).then(function (data) {
            var range = data.range
            var groupMap = {0: {text: 'root'}};
            var permissionMap = {};
            Ext.Array.each(data.group, function (group) {
                groupMap[group.sys_permission_group_id] = group
            });
            Ext.Array.each(data.permission, function (permission) {
                var groupId = permission.sys_permission_group_id
                if (!groupMap[groupId]) {
                    return
                }
                if (!groupMap[groupId].permissions) {
                    groupMap[groupId].permissions = []
                }
                if (range[permission.sys_permission_id]) {
                    permission.sys_permission_range_list = range[permission.sys_permission_id];
                }
                groupMap[groupId].permissions.push(permission)
                permissionMap[permission.sys_permission_id] = permission
            });
            var permissionGroupMap = {};
            for (var groupId in groupMap) {
                var group = groupMap[groupId]
                var pid = group.permission_group_pid
                if (group.permissions) {
                    if (!permissionGroupMap[pid]) {
                        permissionGroupMap[pid] = []
                    }
                    permissionGroupMap[pid].push(group)
                } else {
                    if (groupMap[pid]) {
                        if (!groupMap[pid].children) {
                            groupMap[pid].children = []
                        }
                        groupMap[pid].children.push(group)
                    }
                }
            }
            var firstGroup = null
            for (var idx in groupMap) {
                var group = groupMap[idx]
                if (!group.permissions && !group.children) {
                    group.leaf = true
                    if (!firstGroup) {
                        firstGroup = group
                    }
                }
            }
            me._permissionGroupMap = permissionGroupMap
            me._permissionMap = permissionMap
            me.down('treepanel').setStore(Ext.create('Ext.data.TreeStore', {
                root: groupMap[0]
            }));
            if (firstGroup) {
                me._createPermissionCard(firstGroup.sys_permission_group_id);
            }
        })
    },
    _onGroupItemClick    : function (treePanel, node, item, index, e, eOpts) {
        var me = this
        var groupId = node.get('sys_permission_group_id')
        if (node.get('leaf') && groupId > 0) {
            me._createPermissionCard(groupId);
        }
    },
    _createPermissionCard: function (groupId) {
        let me = this
        var compId = 'permission_group_' + groupId
        var card = me.down('form').getLayout()
        if (me._permissionGroupCard[compId]) {
            item = me._permissionGroupCard[compId]
        } else if (me._permissionGroupMap[groupId]) {
            var items = []
            var groups = me._permissionGroupMap[groupId]
            for (var idx in groups) {
                var group = groups[idx]
                var permissionItems = [];
                var item = {xtype: 'fieldset', margin: '5 10', layout: 'column', title: group['permission_group_name'], items: permissionItems}
                for (var key in group.permissions) {
                    var permission = group.permissions[key]
                    var pitem = {xtype: 'checkboxfield', columnWidth: 0.25, checked: permission.sys_role_permission_id, name: 'permission_ids', boxLabel: permission.sys_permission_name, inputValue: permission.sys_permission_id}
                    if (permission.sys_permission_range_list) {
                        pitem.listeners = {
                            change: function (it, newVal) {
                                var combo = it.nextSibling('combobox');
                                combo.setDisabled(!newVal);
                            }
                        }
                        var container = {
                            xtype: 'container', layout: 'hbox', columnWidth: 0.5, items: [
                                pitem,
                                {
                                    xtype       : 'combobox', margin: '0 0 0 10',
                                    displayField: 'permission_range_name',
                                    valueField  : 'sys_permission_range_id',
                                    queryMode   : 'local',
                                    editable    : false,
                                    allowBlank  : false,
                                    name        : 'range_' + permission.sys_permission_id,
                                    value       : permission.sys_permission_range_id,
                                    disabled    : !permission.sys_role_permission_id,
                                    store       : Ext.create('app.data.store.AbstractLocalStore', {
                                        data: permission.sys_permission_range_list
                                    })
                                }
                            ]
                        }
                        permissionItems.push(container)
                    } else {
                        permissionItems.push(pitem)
                    }
                }
                items.push(item)
            }
            item = Ext.create('Ext.container.Container', {layout: {type: 'vbox', align: 'stretch'}, items: items})
        }
        card.setActiveItem(item);
    },
    _save                : function () {
        var me = this;
        var form = me.down('form');
        if (!form.isValid()) {
            return;
        }
        var roleId = me.$record.get('sys_role_id')
        var values = form.getValues();
        var permissionIds = values.permission_ids || []
        var add = [];
        var upd = [];
        var del = [];
        permissionIds = Ext.isArray(permissionIds) ? permissionIds : [permissionIds]
        for (var idx in me._permissionMap) {
            var permission = me._permissionMap[idx]
            var permissionId = permission.sys_permission_id
            if (permissionIds.includes(permissionId)) {
                var item = {}
                if (permission.sys_permission_range_list && permission.sys_permission_range_list.length > 0) {
                    var rangeKey = 'range_' + permissionId
                    if (!values[rangeKey]) {
                        app.util.Msg.error('权限【' + permission.permission_name + '】必须选择数据范围');
                        return
                    }
                    item.sys_permission_range_id = values[rangeKey]
                }
                if (Ext.isEmpty(permission.sys_role_permission_id)) {
                    item.sys_role_id = roleId
                    item.sys_permission_id = permissionId
                    add.push(item)
                } else if (item.sys_permission_range_id != permission.sys_permission_range_id) {
                    item.sys_role_permission_id = permission.sys_role_permission_id
                    upd.push(item)
                }
            } else if (!Ext.isEmpty(permission.sys_role_permission_id)) {
                del.push(permission.sys_role_permission_id)
            }
        }
        if (add.length <= 0 && upd.length <= 0 && del.length <= 0) {
            app.util.Msg.warn('没有修改，不需要保存');
            return;
        }
        var params = {add: add, upd: upd, del: del, sys_role_id: roleId}
        app.svc.sys.PermissionSvc.saveRolePermission.call(me, params).then(function (permission) {
            me.deferred.resolve(permission)
            me.destroy()
        })
    }
});
