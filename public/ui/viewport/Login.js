Ext.define('app.viewport.login', {
    extend       : 'Ext.container.Container',
    layout       : 'center',
    requires     : ['app.svc.sys.AuthSvc'],
    initComponent: function () {
        var me = this;
        me.items = {
            title      : '用户登录',
            xtype      : 'form',
            border     : 0,
            name       : 'mainForm',
            width      : 450,
            layout     : {
                type : 'vbox',
                align: 'stretch'
            },
            bodyPadding: '20',
            defaults   : {
                xtype              : 'textfield',
                margin             : '0 0 24 0',
                labelWidth         : 60,
                allowBlank         : false,
                allowOnlyWhitespace: false,
                enforceMaxLength   : true
            },
            items      : [
                {
                    name      : 'user_account',
                    maxLength : 11,
                    emptyText : '请输入手机号',
                    regex     : /^1(\d{10})$/,
                    regexText : '手机号格式有误',
                    fieldLabel: '手机号',
                    height    : 30

                },
                {
                    name      : 'user_pwd',
                    inputType : 'password',
                    maxLength : 100,
                    emptyText : '请输入密码',
                    fieldLabel: '密码',
                    height    : 30
                },
                {
                    xtype: 'component',
                    style: {
                        height    : '30px',
                        lineHeight: '30px',
                        textAlign : 'right',
                        fontSize  : '13px'
                    },
                    html : [
                        '首次登录或忘记密码，请<a class="opt-btn" style="text-decoration:underline;" href="/resetPwd.html">重置密码</a>'
                    ]
                },
                {
                    xtype: 'button',
                    scale: 'large',
                    name : 'submitBtn',
                    text : '登录'
                }
            ]
        };
        me.callParent(arguments);
    },
    afterRender  : function () {
        var self = this;
        self.down('button[name=submitBtn]').on('click', self._doSubmit, self)
        self.callParent(arguments);
    },
    _doSubmit    : function () {
        var self = this;
        var form = self.down('form');
        if (!form.isValid()) {
            return;
        }
        var values = form.getValues();
        app.svc.sys.AuthSvc.loginByPwd.call(self, values.user_account, values.user_pwd).then(function () {
            document.location = '/index.html';
        })
    }
});
