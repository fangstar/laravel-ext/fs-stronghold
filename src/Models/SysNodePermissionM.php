<?php

namespace Fstar\Stronghold\Models;

class SysNodePermissionM extends BaseModel {
    protected $table = 'sys_node_permission';
    protected $primaryKey = 'sys_node_permission_id';
}