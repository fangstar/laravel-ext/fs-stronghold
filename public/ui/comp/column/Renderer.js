Ext.define('app.comp.column.Renderer', {
    requires: ['app.util.DateUtil'],
    statics : {
        numberRenderer  : function (value, defaultVal, unit, decimalPrecision) {
            if (Ext.isEmpty(value) || !Ext.isNumeric(value)) {
                return defaultVal;
            }
            value = Number(value);
            if (isNaN(value)) {
                return defaultVal;
            }
            if (!Ext.isString(unit)) {
                unit = '';
            }
            if (Ext.isNumeric(decimalPrecision) && decimalPrecision >= 0) {
                value = value.toFixed(decimalPrecision);
            }
            return value + '' + unit;
        },
        amountRenderer  : function (value) {
            if (Ext.isNumeric(value)) {
                return '￥' + Ext.util.Format.number(value, '0,000.00');
            }
            return null;
        },
        dateTimeRenderer: function (value) {
            return app.util.DateUtil.dateFormat(value, 'Y-m-d H:i:s') || '';
        },
        dateRenderer    : function (value) {
            return app.util.DateUtil.dateFormat(value, 'Y-m-d') || '';
        }
    }
})