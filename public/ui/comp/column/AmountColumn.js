/**
 * 数字列.
 */
Ext.define('app.comp.column.AmountColumn', {
    extend         : 'Ext.grid.column.Column',
    alias          : ['widget.amountcolumn', 'widget.AmountColumn'],
    align          : 'right',
    width          : 100,
    emptyText      : '',
    format         : '0,000.00',
    defaultRenderer: function (v, cellValues, record, rowIdx, colIdx, store, view) {
        if (cellValues) {
            cellValues.style = 'padding-right: 5px;';
        }
        if (Ext.isEmpty(v)) {
            return this.emptyText;
        }
        return '￥' + Ext.util.Format.number(v, '0,000.00');
    }
});
