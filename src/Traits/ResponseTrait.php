<?php

namespace Fstar\Stronghold\Traits;

trait ResponseTrait {

    protected function resp(mixed $data, int $httpCode = 200, string $contentType = 'application/json') {
        $emp_id = session('emp_id', null);
        $resp = response($data, $httpCode)
            ->header('Content-Type', $contentType);
        if(!empty($emp_id)) {
            $authorization = $this->createAuthorization();
            $resp->header('Authorization', $authorization)
                 ->cookie(cookie('auth-token', $authorization));
        }
        return $resp;
    }

    protected function respSuccessMsg(string $msg) {
        $arr = $this->initRespData();
        $arr[config('fstar-permission.resp.field.msg', 'msg')] = $msg;
        return $this->resp($arr);
    }

    protected function respSuccessData(mixed $data = null, string $msg = "操作成功") {
        $resp = config('fstar-permission.resp');
        $arr = $this->initRespData();
        $arr[data_get($resp, 'field.msg', 'msg')] = $msg;
        $arr[data_get($resp, 'field.data', 'data')] = $data;
        return $this->resp($arr);
    }

    protected function respSuccessDataTotal(array $data_total = ['data' => [], 'total' => 0], $msg = "查询成功") {
        $resp = config('fstar-permission.resp');
        $arr = $this->initRespData();
        $arr[data_get($resp, 'field.msg', 'msg')] = $msg;
        $arr[data_get($resp, 'field.data', 'data')] = $data_total['data'];
        $arr[data_get($resp, 'field.total', 'total')] = $data_total['total'];
        return $this->resp($arr);
    }

    protected function respSuccessDataTotalPage(array $data_total = ['data' => [], 'total' => 0], $page = [], $msg = "查询成功") {
        $resp = config('fstar-permission.resp');
        $arr = $this->initRespData();
        $arr[data_get($resp, 'field.msg', 'msg')] = $msg;
        $arr[data_get($resp, 'field.data', 'data')] = $data_total['data'];
        $arr[data_get($resp, 'field.total', 'total')] = $data_total['total'];
        $arr[data_get($resp, 'field.page', 'page')] = $page;
        return $this->resp($arr);
    }

    protected function respSuccess(array $data = [], string $msg = "查询成功") {
        $resp = config('fstar-permission.resp');
        $arr = $this->initRespData();
        $arr[data_get($resp, 'field.msg', 'msg')] = $msg;
        $arr[data_get($resp, 'field.data', 'data')] = $data;
        return $this->resp($arr);
    }

    protected function respSOM(string $msg = "查询成功") {
        $arr = $this->initRespData();
        $arr[config('fstar-permission.resp.field.msg', 'msg')] = $msg;
        return $this->resp($arr);
    }

    protected function respError(string $msg = "系统出现异常，可拨打电话：400-0707566，或让经理在微信群中@刘宇。", int|string $error_code = 0, mixed $data = null) {
        $arr = $this->initRespData();
        $resp = config('fstar-permission.resp');
        $arr[data_get($resp, 'field.success', 'success')] = data_get($resp, 'val.error', false);
        $arr[data_get($resp, 'field.msg', 'msg')] = $msg;
        $arr[data_get($resp, 'field.data', 'data')] = $data;
        $arr[data_get($resp, 'field.errorcode', 'errorcode')] = $error_code;
        return $this->resp($arr);
    }

    protected function respDownload(string $filePath, string $fileName, string $contentType, bool $deleteFileAfterSend = true) {
        $headers = [
            'Content-Type: '.$contentType,
            'token: '.encrypt(session()->getId())
        ];
        return response()
            ->download($filePath, $fileName, $headers)
            ->deleteFileAfterSend($deleteFileAfterSend);
    }

    protected function createAuthorization() {
        return encrypt(session()->getId().'|'.time());
    }

    protected function initRespData(): array {
        $arr = [];
        $resp = config('fstar-permission.resp');
        $arr[data_get($resp, 'field.success', 'success')] = data_get($resp, 'val.success', true);
        $arr[data_get($resp, 'field.errorcode', 'errorcode')] = data_get($resp, 'val.errorcode', 0);
        $arr[data_get($resp, 'field.msg', 'msg')] = '';
        $arr[data_get($resp, 'field.data', 'data')] = [];
        return $arr;
    }
}
