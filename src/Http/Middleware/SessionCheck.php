<?php

/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

namespace Fstar\Stronghold\Http\Middleware;

use Closure;
use Fstar\Stronghold\Exception\FsStrongholdException;
use Fstar\Stronghold\Traits\ResponseTrait;
use Illuminate\Http\Request;

class SessionCheck {

    use ResponseTrait;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    private $expect = [

    ];


    public function handle(Request $request, Closure $next) {
        $err_code_no_login = config('fstar-stronghold.err_code.no_login');
        $path = $request->path();
        $now = time();
        $token = $request->header('Authorization', $request->get('auth-token'));
        $skpi = in_array($path, $this->expect);
        if(!empty($token)) {
            $data_arry = explode('|', decrypt($token));
            $data_cnt = count($data_arry);
            if($data_cnt != 2) {
                if($skpi) {
                    return $next($request);
                }
                throw new FsStrongholdException("Authorization 认证错误");
            }
            $session_id = $data_arry[0];
            $last_request_time = $data_arry[1];
            $session_timeout_at = config('session.lifetime', 30) * 60 + $last_request_time;
            if($session_timeout_at < $now) {
                if($skpi) {
                    return $next($request);
                }
                throw new FsStrongholdException("安全认证已过期，请重新登录认证", $err_code_no_login);
            }
            session()->setId($session_id);
            session()->start();
        }
        if($skpi) {
            return $next($request);
        }
        if(!session('is_login', false)) {
            return $this->respError("登录超时或未登录，请重新登录", $err_code_no_login);
        }
        return $next($request);
    }
}
