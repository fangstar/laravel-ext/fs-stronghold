<?php

namespace Fstar\Stronghold\Models;

class SysRouteM extends BaseModel {
    protected $table = 'sys_route';
    protected $primaryKey = 'sys_route_id';
}