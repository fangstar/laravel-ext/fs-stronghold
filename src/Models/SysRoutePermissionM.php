<?php

namespace Fstar\Stronghold\Models;

class SysRoutePermissionM extends BaseModel {
    protected $table = 'sys_route_permission';
    protected $primaryKey = 'sys_route_permission_id';
}