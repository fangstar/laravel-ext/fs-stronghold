<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_role_permission', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('角色权限表');
            $table->id('sys_role_permission_id')->comment('角色权限ID');
            $table->integer('sys_permission_id')->index('idx_sys_permission_id')->comment('权限ID');
            $table->integer('sys_role_id')->index('idx_sys_role_id')->comment('角色ID');
            $table->integer('sys_permission_range_id')->nullable()->comment('权限范围ID');
            $table->integer('sys_role_permission_del_rel_id')->default(0)->comment('删除辅助字段');
            $table->tinyInteger('delete_flag')->default(0)->comment('删除状态 0:正常  1:软删  2:删除');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->bigInteger('deleted_at')->nullable()->comment('删除时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
            $table->integer('delete_id')->nullable()->comment('删除人');
            $table->unique(['sys_permission_id', 'sys_role_id', 'delete_flag', 'sys_role_permission_del_rel_id'], 'idx_role_permission_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
