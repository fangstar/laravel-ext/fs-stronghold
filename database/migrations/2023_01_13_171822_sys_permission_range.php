<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_permission_range', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('权限数据范围表');
            $table->id('sys_permission_range_id')->comment('权限数据范围ID');
            $table->string('permission_range_key', 31)->comment('范围key');
            $table->string('permission_range_name', 31)->comment('范围名称');
            $table->integer('permission_range_level')->comment('权限等级  由小到大  数值越大级别越高');
            $table->string('permission_range_desc', 63)->nullable()->comment('范围描述');
            $table->tinyInteger('delete_flag')->default(0)->comment('删除状态 0:正常  1:软删  2:删除');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->bigInteger('deleted_at')->nullable()->comment('删除时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
            $table->integer('delete_id')->nullable()->comment('删除人');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
