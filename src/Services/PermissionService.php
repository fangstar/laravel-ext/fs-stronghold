<?php

namespace Fstar\Stronghold\Services;

use Fstar\Stronghold\Constants\FsConstant;
use Fstar\Stronghold\Exception\FsStrongholdException;
use Fstar\Stronghold\Models\SysNodeM;
use Fstar\Stronghold\Models\SysPermissionGroupM;
use Fstar\Stronghold\Models\SysPermissionM;
use Fstar\Stronghold\Models\SysPermissionRangeM;
use Fstar\Stronghold\Models\SysRoleM;
use Fstar\Stronghold\Models\SysRolePermissionM;
use Fstar\Stronghold\Models\SysUserRoleM;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PermissionService {
    public function getEmpMenus($user_id, $node_cate) {
        $node_map = SysRoleM::join('sys_user_role as ur', 'ur.sys_role_id', '=', 'sys_role.sys_role_id')
                            ->join('sys_role_permission as rp', 'rp.sys_role_id', '=', 'ur.sys_role_id')
                            ->join('sys_permission as p', 'p.sys_permission_id', '=', 'rp.sys_permission_id')
                            ->join('sys_node_permission as np', 'np.sys_permission_id', '=', 'p.sys_permission_id')
                            ->join('sys_node as n', 'n.sys_node_id', '=', 'np.sys_node_id')
                            ->where('ur.sys_user_id', $user_id)
                            ->where('ur.delete_flag', FsConstant::DEL_NO)
                            ->where('rp.delete_flag', FsConstant::DEL_NO)
                            ->where('np.delete_flag', FsConstant::DEL_NO)
                            ->where('n.delete_flag', FsConstant::DEL_NO)
                            ->where('n.node_cate', $node_cate)
                            ->where('sys_role.delete_flag', FsConstant::DEL_NO)
                            ->where('p.permission_status', SysPermissionM::STATUS_ENABLE)
                            ->orderBy('n.node_order')
                            ->get(['n.sys_node_id', 'n.node_pid', 'n.node_name', 'n.node_cate', 'n.node_route_path', 'n.node_icon_cls',
                                   'n.node_icon_url', 'n.node_type', 'n.node_is_leaf', 'n.node_conf', 'n.node_desc', 'n.node_order'])
                            ->pluck(null, 'sys_node_id')
                            ->toArray();
        $node_ids = Arr::pluck($node_map, 'node_pid');
        $pnode_map = SysNodeM::whereIn('sys_node_id', $node_ids)->get()->pluck(null, 'sys_node_id')->toArray();
        return array_values(array_merge($node_map, $pnode_map));
    }

    public function roleList() {
        return SysRoleM::leftJoin('sys_role_permission as rp', function($join) {
            $join->on('rp.sys_role_id', '=', 'sys_role.sys_role_id')
                 ->where('rp.delete_flag', FsConstant::DEL_NO);
        })
                       ->leftJoin('sys_user_role as ur', function($join) {
                           $join->on('ur.sys_role_id', '=', 'sys_role.sys_role_id')
                                ->where('ur.delete_flag', FsConstant::DEL_NO);
                       })
                       ->where('sys_role.delete_flag', FsConstant::DEL_NO)
                       ->groupBy('sys_role.sys_role_id')
                       ->selectRaw('sys_role.*,count(distinct rp.sys_permission_id) as permission_cnt,count(distinct sys_user_role_id) as user_cnt')
                       ->get()
                       ->toArray();
    }

    public function roleAdd($role, $user_id) {
        $role['role_cate'] = SysRoleM::CATE_USER;
        $role['create_id'] = $user_id;
        return SysRoleM::create($role);
    }

    public function roleUpdate($role, $user_id) {
        $roleM = SysRoleM::queryByIdNoDel($role['sys_role_id']);
        $role['update_id'] = $user_id;
        $role['role_cate'] = SysRoleM::CATE_USER;
        $roleM->fill($role);
        $roleM->save();
        return Arr::only($roleM->toArray(), ['sys_role_id', 'role_name', 'role_desc', 'updated_at']);
    }

    public function roleDel($role_id, $user_id) {
        $roleM = SysRoleM::queryByIdNoDel($role_id);
        if($roleM->role_cate == SysRoleM::CATE_SYS) {
            throw new FsStrongholdException("不能删除系统角色");
        }
        $del_fields = SysRoleM::createSoftDelField($user_id);
        $roleM->fill($del_fields);
        $conn = SysRoleM::getConn();
        $conn->beginTransaction();
        try {
            $roleM->save();
            SysUserRoleM::where('sys_role_id', $role_id)->where('delete_flag', FsConstant::DEL_NO)->update($del_fields);
            SysRolePermissionM::where('sys_role_id', $role_id)->where('delete_flag', FsConstant::DEL_NO)->update($del_fields);
            $conn->commit();
        } catch(\Exception $ex) {
            $conn->rollBack();
            throw $ex;
        }
        return $roleM;
    }

    public function getPermissionByRoleId($role_id) {
        $permission_group = SysPermissionGroupM::deleteNo()
                                               ->orderBy('permission_group_order')
                                               ->get(['sys_permission_group_id', 'permission_group_pid', 'permission_group_name'])
                                               ->toArray();
        $permission_range = SysPermissionRangeM::join('sys_permission_range_rel as rel', 'rel.sys_permission_range_id', '=', 'sys_permission_range.sys_permission_range_id')
                                               ->where('rel.delete_flag', FsConstant::DEL_NO)
                                               ->where('sys_permission_range.delete_flag', FsConstant::DEL_NO)
                                               ->orderBy('permission_range_level', 'asc')
                                               ->get(['sys_permission_id', 'permission_range_key', 'rel.sys_permission_range_id', 'permission_range_name', 'permission_range_level', 'permission_range_desc'])
                                               ->groupBy('sys_permission_id')
                                               ->toArray();
        $permission = SysPermissionM::leftJoin('sys_role_permission as rp', function($join) use ($role_id) {
            $join->on('rp.sys_permission_id', '=', 'sys_permission.sys_permission_id')
                 ->where('rp.sys_role_id', $role_id)
                 ->where('rp.delete_flag', FsConstant::DEL_NO);
        })
                                    ->where('sys_permission.permission_status', SysPermissionM::STATUS_ENABLE)
                                    ->get(['sys_permission.*', 'rp.sys_permission_range_id', 'rp.sys_role_permission_id']);
        return ['group' => $permission_group, 'range' => $permission_range, 'permission' => $permission];

    }

    public function saveRolePermission($role_id, $add, $upd, $del, $user_id) {
        $now = time();
        $permission_add = [];
        $range_add = [];
        foreach($add as $idx => $item) {
            $item['create_id'] = $user_id;
            $item['created_at'] = $now;
            if(array_key_exists('sys_permission_range_id', $item)) {
                $range_add[] = $item;
            } else {
                $permission_add[] = $item;
            }
        }
        $conn = SysRolePermissionM::getConn();
        $conn->beginTransaction();
        try {
            if(count($permission_add) > 0) {
                SysRolePermissionM::insert($permission_add);
            }
            if(count($range_add) > 0) {
                SysRolePermissionM::insert($range_add);
            }
            foreach($upd as $idx => $item) {
                SysRolePermissionM::where('sys_role_permission_id', $item['sys_role_permission_id'])
                                  ->update(['sys_permission_range_id' => data_get($item, 'sys_permission_range_id', null), 'update_id' => $user_id]);
            }
            if(count($del) > 0) {
                SysRolePermissionM::whereIn('sys_role_permission_id', $del)
                                  ->update([
                                               'delete_flag'                    => FsConstant::DEL_YES,
                                               'delete_id'                      => $user_id,
                                               'deleted_at'                     => $now,
                                               'sys_role_permission_del_rel_id' => DB::raw('sys_role_permission_id')
                                           ]);
            }
            $conn->commit();
        } catch(\Exception $ex) {
            $conn->rollBack();
            throw $ex;
        }
        $permission_cnt = SysRolePermissionM::where('sys_role_id', $role_id)->deleteNo()->count('sys_role_permission_id');
        return ['permission_cnt' => $permission_cnt];
    }
}