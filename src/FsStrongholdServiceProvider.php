<?php

namespace Fstar\Stronghold;

use Illuminate\Support\ServiceProvider;

class FsStrongholdServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {
        // Publish the configuration file
        $this->publishes([__DIR__."/../config/fstar-stronghold.php" => config_path(Constants::conf_name.".php")]);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->publishes([__DIR__.'/../public/' => public_path()], 'stronghold-public');
    }

    /**
     * 在容器中注册绑定。
     *
     * @return void
     */
    public function register() {
        $this->mergeConfigFrom(__DIR__."/../config/fstar-stronghold.php", Constants::conf_name);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return [
            Constants::lib_stronghold
        ];
    }
}