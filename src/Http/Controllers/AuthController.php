<?php

namespace Fstar\Stronghold\Http\Controllers;

use Fstar\Stronghold\Exception\FsStrongholdException;
use Fstar\Stronghold\Services\AuthService;

class AuthController extends BaseController {

    public function loginByPwd(AuthService $svc) {
        $params = request()->all();
        $validator = validator($params,
                               [
                                   'user_account' => 'required',
                                   'user_pwd'     => 'required'
                               ],
                               [
                                   'required' => '缺少:attribute'
                               ],
                               [
                                   "user_account" => "用户账号",
                                   "user_pwd"     => "用户密码"
                               ]);
        if($validator->fails()) {
            throw new FsStrongholdException($validator->errors()->first());
        }
        $data = $svc->loginByPwd($params['user_account'], $params['user_pwd']);
        return $this->respSuccessData($data);
    }

    public function hasLogin() {
        $has_login = session('is_login', false);
        return $this->respSuccessData($has_login);
    }

    public function logout(AuthService $svc) {
        session()->flush();
        return $this->respSuccessMsg('退出登录成功');
    }

    public function getLoginUser(AuthService $svc) {
        $data = $svc->getLoginUser();
        if(empty($data['is_login'])){
            $err_code_no_login = config('fstar-stronghold.err_code.no_login');
            return $this->respError("登录超时或未登录，请重新登录", $err_code_no_login);
        }
        return $this->respSuccessData($data);
    }
}