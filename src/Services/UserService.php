<?php

namespace Fstar\Stronghold\Services;

use Fstar\Stronghold\Models\SysUserConfM;

class UserService {
    public function getUserConf($user_id, $conf_fields = ['*']) {
        return SysUserConfM::where('sys_user_id', $user_id)->first($conf_fields);
    }
}