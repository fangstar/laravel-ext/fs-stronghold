<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('hr_emp', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('员工表');
            $table->id('emp_id')->comment('员工ID和sys_user.sys_user_id是同一个值');
            $table->integer('dept_id')->comment('部门ID');
            $table->integer('position_id')->nullable()->comment('职务ID');
            $table->string('emp_no', 31)->unique('idx_emp_no_unique')->comment('工号');
            $table->string('emp_name', 31)->comment('姓名');
            $table->string('emp_mobile', 31)->comment('电话');
            $table->integer('emp_img_host_id')->nullable()->comment('员工图片存储的HOST配置ID');
            $table->string('emp_img_url', 127)->nullable()->comment('员工图片地址');
            $table->tinyInteger('emp_sex')->nullable()->comment('性别 1：男  2：女');
            $table->string('emp_id_num', 31)->nullable()->comment('身份证号码');
            $table->bigInteger('emp_entry_at')->nullable()->comment('入职日期');
            $table->bigInteger('emp_leave_at')->nullable()->comment('离职日期');
            $table->tinyInteger('emp_status')->default(1)->comment('员工状态 1：初始化  20：待入职  21：入职 30：待离职 31：离职');
            $table->string('emp_memo', 63)->nullable()->comment('备注');

            $table->tinyInteger('delete_flag')->default(0)->comment('删除状态 0:正常  1:软删  2:删除');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->bigInteger('deleted_at')->nullable()->comment('删除时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
            $table->integer('delete_id')->nullable()->comment('删除人');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
