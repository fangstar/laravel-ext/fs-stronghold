Ext.define('app.data.store.SmartSearch', {
    smartSearch    : function (keyword, fields) {
        var me = this
        me.clearFilter();
        if (!Ext.isEmpty(keyword)) {
            var reg = new RegExp(keyword, 'gi');
            me.filterBy(function (rec) {
                for (var idx in fields) {
                    var text = rec.get(fields[idx]) || '';
                    try {
                        reg.test()
                        if (text && reg.test(text)) {
                            return true
                        }
                    } catch (err) {

                    }
                }
                return false;
            });
        }
    },
    gridSmartSearch: function (keyword, grid) {
        var store = me;
        var columns = grid.getColumns();
        store.clearFilter();
        if (!Ext.isEmpty(word)) {
            var reg = new RegExp(word, 'gi');
            store.filterBy(function (rec) {
                for (var i = 0; i < columns.length; i++) {
                    var col = columns[i];
                    var dataIndex = col['dataIndex'];
                    if (col.isHidden()) {
                        continue;
                    }
                    var text = rec.get(dataIndex);
                    if (Ext.isFunction(col.renderer)) {
                        text = col.renderer(text, {}, rec);
                    }
                    if (text && text.match && text.match(reg)) {
                        return true;
                    }
                }
                return false;
            });
        }
    }
})