Ext.define('app.util.DateUtil', {
    statics: {
        dateFormat: function (second, format, emptyText) {
            if (!second || second == '0') {
                return emptyText;
            }
            var date = new Date();
            date.setTime(Number(second + '000'));
            if (date == 'Invalid Date') {
                return emptyText;
            }
            format = format || 'Y-m-d';
            return Ext.Date.format(date, format);
        }
    }
})