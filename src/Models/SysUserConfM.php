<?php

namespace Fstar\Stronghold\Models;

class SysUserConfM extends BaseModel {
    protected $table = 'sys_user_conf';
    protected $primaryKey = 'sys_user_conf_id';
}