# Fstar Stronghold

#### 介绍
房星科技基础架构

#### 软件架构
laravel


#### 安装教程

1. composer require fstar/stronghold
2. php artisan vendor:publish --provider=Fstar\Stronghold\FsStrongholdServiceProvider
3. .env 文件增加配置
``` php
DB_HOST=
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
   
REDIS_PREFIX=
REDIS_CLIENT=predis
REDIS_HOST=
REDIS_PASSWORD=
REDIS_PORT=6379
REDIS_DB=31
REDIS_DB_CACHE=32
REDIS_SESSION_DB=33
```
4.增加会话检查中间件 app/Http/Kernel.php  $routeMiddleware
``` php
'session_check' => \Fstar\Stronghold\Http\Middleware\SessionCheck::class,
```
5.执行migrate创建数据表
``` php
php .\artisan migrate
```

#### 使用说明
###### 

#### 参与贡献

1.  房星科技