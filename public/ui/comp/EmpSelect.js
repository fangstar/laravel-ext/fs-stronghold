Ext.define('app.comp.EmpSelect', {
    extend       : 'Ext.window.Window',
    mixins       : ['app.comp.mixin.CompDeferred'],
    minWidth     : 800,
    minHeight    : 400,
    $empSvc      : null,
    $savedEmpSvc : null,
    layout       : {
        type : 'hbox',
        align: 'stretch'
    },
    initComponent: function () {
        var me = this;
        me.items = [
            {
                xtype: 'grid', flex: 1, columns: [
                    {text: '工号', dataIndex: 'emp_no'},
                    {text: '姓名', dataIndex: 'emp_name'},
                    {text: '手机号', dataIndex: 'emp_mobile'}
                ]
            },
            {
                xtype: 'panel', border: 1, width: '30', layout: 'vbox', items: [
                    {xtype: 'container', flex: 1},
                    {xtype: 'button', iconCls: ''},
                    {xtype: 'container', flex: 1}
                ]
            },
            {
                xtype: 'grid', flex: 1, columns: [
                    {text: '工号', dataIndex: 'emp_no'},
                    {text: '姓名', dataIndex: 'emp_name'},
                    {text: '手机号', dataIndex: 'emp_mobile'}
                ]
            }
        ];

        me.callParent(arguments);
    }
})