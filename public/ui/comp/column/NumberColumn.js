/**
 * 数字列.
 */
Ext.define('app.comp.column.NumberColumn', {
    extend          : 'Ext.grid.column.Column',
    alias           : ['widget.ftnumbercolumn', 'widget.FtNumberColumn'],
    align           : 'right',
    width           : 100,
    unit            : '',
    emptyText       : '',
    requires        : ['app.comp.column.Renderer'],
    decimalPrecision: 'auto',
    defaultRenderer : function (v, meta, rec) {
        var self = this;
        if (meta) {
            meta.style = 'padding-right: 5px;';
        }
        return app.comp.column.Renderer.numberRenderer(v, self.emptyText, self.unit, self.decimalPrecision);
    }
});
