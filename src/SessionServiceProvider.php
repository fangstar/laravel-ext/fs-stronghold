<?php

namespace Fstar\Stronghold;

use Fstar\Stronghold\Extensions\RedisSessionDriver;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class SessionServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Session::extend('redis_ext', function($app) {
            return new RedisSessionDriver();
        });
    }
    
}
