<?php

namespace Fstar\Stronghold\Models;

class SysNodeM extends BaseModel {
    protected $table = 'sys_node';
    protected $primaryKey = 'sys_node_id';
    
    const CATE_CLIENT = 1;
    const CATE_MANAGE = 2;
}