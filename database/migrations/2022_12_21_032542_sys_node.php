<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_node', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('系统节点表');
            $table->id('sys_node_id')->comment('节点ID');
            $table->integer('node_pid')->default(0)->comment('节点父ID');
            $table->string('node_name', 31)->comment('节点名称');
            $table->tinyInteger('node_cate')->default(1)->comment('节点类别  1：用户端  2：管理端');
            $table->string('node_route_path', 127)->nullable()->comment('页面路由地址');
            $table->string('node_icon_cls', 31)->nullable()->comment('图标类');
            $table->string('node_icon_url', 127)->nullable()->comment('图标地址');
            $table->string('node_type', 31)->default('page')->comment('节点类型');
            $table->tinyInteger('node_is_leaf')->default(0)->comment('是否是叶子节点');
            $table->string('node_conf', 255)->nullable()->comment('节点参数配置');
            $table->string('node_desc', 63)->nullable()->comment('节点描述');
            $table->integer('node_order')->default(1)->comment('节点排序 由小到大');
            $table->tinyInteger('delete_flag')->default(0)->comment('删除状态 0:正常  1:软删  2:删除');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->bigInteger('deleted_at')->nullable()->comment('删除时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
            $table->integer('delete_id')->nullable()->comment('删除人');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
