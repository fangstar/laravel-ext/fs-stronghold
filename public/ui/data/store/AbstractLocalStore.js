/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

/**
 * 数据表store的抽象类
 * 如果需要使用model的话请使用columns属性，该属性即为model中的fields属性
 * @param {type} param1
 * @param {type} param2
 */
Ext.define('app.data.store.AbstractLocalStore', {
    extend: 'Ext.data.Store',
    fields: ['id'],
    mixins: ['app.data.store.SmartSearch'],
    /**
     * 默认为包含被过滤的记录
     * @param {type} noFilter
     * @returns {Array|Array.getAllRecords.records}
     */
    getAllRecords: function (noFilter) {
        var self = this;
        var records = [];
        if (noFilter) {
            self.each(function (record) {
                records.push(record);
            });
        } else {
            self.queryBy(function (record, id) {
                records.push(record);
                return true;
            });
        }
        return records;
    },
    /**
     *
     * @param {type} excludeFilter 是否排除被过滤的数据
     * @returns {undefined}
     */
    getAllRawData: function (excludeFilter) {
        var self = this;
        var iterp = [];
        if (excludeFilter) {
            self.each(function (record) {
                iterp.push(record.raw);
            });
        } else {
            self.queryBy(function (record, id) {
                iterp.push(record.raw);
                return true;
            });
        }
        return iterp;
    }
});