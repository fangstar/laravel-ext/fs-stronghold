<?php

namespace Fstar\Stronghold\Models;

class HrEmpM extends BaseModel {
    protected $table = 'hr_emp';
    protected $primaryKey = 'emp_id';
}