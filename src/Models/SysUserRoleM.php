<?php

namespace Fstar\Stronghold\Models;

class SysUserRoleM extends BaseModel {
    protected $table = 'sys_user_role';
    protected $primaryKey = 'sys_user_role_id';
}