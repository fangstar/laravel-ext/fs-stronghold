<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_route_permission', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('路由权限表');
            $table->id('sys_route_permission_id')->comment('路由权限ID');
            $table->integer('sys_permission_id')->index('idx_sys_permission_id')->comment('权限ID');
            $table->integer('sys_route_id')->index('idx_sys_route_id')->comment('路由ID');
            $table->tinyInteger('delete_flag')->default(0)->comment('删除状态 0:正常  1:软删  2:删除');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->bigInteger('deleted_at')->nullable()->comment('删除时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
            $table->integer('delete_id')->nullable()->comment('删除人');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
