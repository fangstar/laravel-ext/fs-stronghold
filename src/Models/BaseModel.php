<?php

namespace Fstar\Stronghold\Models;

use Fstar\Stronghold\Constants\FsConstant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Fstar\Stronghold\Exception\NotFoundException;

class BaseModel extends Model {
    protected $connection = 'mysql';
    protected $dateFormat = 'U';
    protected $casts = [
        'created_at' => 'datetime:U',
        'updated_at' => 'datetime:U',
        'deleted_at' => 'datetime:U',
    ];

    public function scopeDeleteYes($query) {
        $query->where('delete_flag', FsConstant::DEL_YES);
    }

    public function scopeDeleteNo($query) {
        $query->where('delete_flag', FsConstant::DEL_NO);
    }

    public static function getTableName() {
        $instance = new static;
        return $instance->getTable();
    }

    public static function getConn() {
        $instance = new static;
        return DB::connection($instance->getConnectionName());
    }

    public static function insertInto($builder, $columns = null) {
        $me = new static();
        $select_sql = $builder->toSql();
        if(is_array($columns)) {
            $columns = implode(',', $columns);
        }
        if(empty($columns)) {
            $insert_sql = "INSERT INTO {$me->table} {$select_sql}";
        } else {
            $insert_sql = "INSERT INTO {$me->table} ({$columns}) {$select_sql}";
        }
        return DB::connection($me->connection)->insert($insert_sql, $builder->getBindings());
    }

    public static function queryById($id, $name = '') {
        $me = new static();
        $model = self::where($me->primaryKey, $id)->first();
        if($model == null) {
            throw new NotFoundException("{$name}信息不存在,{$name}ID:{$id}");
        }
        return $model;
    }

    public static function queryByIdNoDel($id, $name = '') {
        $model = self::queryById($id, $name);
        if($model->delete_flag != FsConstant::DEL_NO) {
            throw new NotFoundException("{$name}信息已经被删除,{$name}ID:{$id}");
        }
        return $model;
    }

    public static function createSoftDelField($del_user_id, $time = null) {
        return [
            'delete_flag' => FsConstant::DEL_YES,
            'delete_id'   => $del_user_id,
            'deleted_at'  => empty($time) ? time() : $time
        ];
    }
}