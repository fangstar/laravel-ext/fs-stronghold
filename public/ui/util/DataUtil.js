Ext.define('app.util.DataUtil', {
    statics: {
        uuid: function () {
            return Ext.data.identifier.Uuid.Global.generate().replaceAll('-', '');
        }
    }
})