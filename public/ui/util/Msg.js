Ext.define('app.util.Msg', {
    statics: {
        info        : function (content) {
            Ext.Msg.show({title: '提示', msg: content, buttonText: {yes: '确定'}, buttons: Ext.Msg.YES, icon: Ext.Msg.INFO});
        },
        warn        : function (content) {
            Ext.Msg.show({title: '提醒', msg: content, buttonText: {yes: '确定'}, buttons: Ext.Msg.YES, icon: Ext.Msg.WARNING});
        },
        error       : function (content) {
            Ext.Msg.show({title: '错误', msg: content, buttonText: {yes: '确定'}, buttons: Ext.Msg.YES, icon: Ext.Msg.ERROR});
        },
        question    : function (content) {
            Ext.Msg.show({title: '错误', msg: content, buttonText: {yes: '确定'}, buttons: Ext.Msg.YES, icon: Ext.Msg.QUESTION});
        },
        confirm     : function (title, content) {
            return new Ext.Promise(function (resolve, reject) {
                Ext.Msg.confirm(title, content, function (buttonId, text, opt) {
                    if (['ok', 'yes'].includes(buttonId)) {
                        resolve(text);
                    } else {
                        reject();
                    }
                })
            })
        },
        toast       : function () {
            var opt = {};
            if (Ext.isEmpty(conf)) {
                opt['html'] = '操作成功';
            } else if (Ext.isString(conf)) {
                opt['html'] = conf;
            }
            opt = Ext.apply({
                                closeToolText  : '关闭',
                                title          : '提示',
                                height         : 100,
                                width          : 200,
                                align          : 't',
                                autoDestroy    : true,
                                slideInDuration: 200,
                                closable       : true
                            }, opt);
            return Ext.toast(opt);
        },
        successToast: function () {
            app.util.Msg.successToast('操作成功')
        }
    }
})