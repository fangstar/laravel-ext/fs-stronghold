/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

Ext.define('app.comp.Topic', function () {
    var topics = {};
    return {
        requires: ['app.util.DataUtil'],
        statics : {
            topics                    : function () {
                return topics;
            },
            _remove                   : function (item) {
                if (topics[item.topic] && Ext.isArray(topics[item.topic])) {
                    var index = Ext.Array.each(topics[item.topic], function (rawItem, i) {
                        if (rawItem.itemId === item.itemId) {
                            return false;
                        }
                    });
                    if (Ext.isNumber(index) && index >= 0) {
                        Ext.Array.removeAt(topics[item.topic], index);
                    }
                    if (topics[item.topic].length === 0 && Ext.isEmpty(topics[item.topic].lastParams)) {
                        delete topics[item.topic];
                    }
                }
            },
            publishCallComponentMethod: function (cmpClassName, method, args) {
                app.comp.Topic.publish('CallComponentMethod::' + cmpClassName, {method: method, args: args});
            },
            publish                   : function () {
                if (arguments.length <= 0) {
                    return;
                }
                if (!(arguments[0] in topics)) {
                    topics[arguments[0]] = [];
                }
                var topicArry = topics[arguments[0]];
                var args = [];
                if (arguments.length > 1) {
                    args.push(arguments[1]);
                }
                if (arguments.length > 2 && arguments[2]) {
                    topicArry.lastParams = args;
                }
                for (var index = 0; index < topicArry.length; index++) {
                    try {
                        topicArry[index].fn.apply(topicArry[index].context, args);
                    } catch (e) {
                        console.error(e);
                    }

                }
            },
            subscribe                 : function (topic, fn, context, getLast) {
                if (Ext.isEmpty(topic)) {
                    Ext.Msg.show({
                                     title  : '错误',
                                     msg    : '事件订阅的TOPIC不能为空',
                                     buttons: Ext.Msg.YES,
                                     icon   : Ext.Msg.ERROR
                                 });
                    return;
                }

                if (!(topic in topics)) {
                    topics[topic] = [];
                }
                var item = {topic: topic, fn: fn, itemId: app.util.DataUtil.uuid()};
                item.context = context || item;
                topics[topic].push(item);
                if (getLast) {
                    fn.apply(context, topics[topic].lastParams);
                }
                return {
                    item  : item,
                    remove: function () {
                        app.comp.Topic._remove(this.item);
                    }
                };
            }
        }
    };
});