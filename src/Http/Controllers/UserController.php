<?php

namespace Fstar\Stronghold\Http\Controllers;

use Fstar\Stronghold\Services\UserService;

class UserController extends BaseController {
    public function getUserConf(UserService $svc) {
        $user_id = session('sys_user_id');
        $userM = $svc->getUserConf($user_id);
        return $this->respSuccessData($userM);
    }
}