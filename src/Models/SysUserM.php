<?php

namespace Fstar\Stronghold\Models;

class SysUserM extends BaseModel {
    protected $table = 'sys_user';
    protected $primaryKey = 'sys_user_id';

    const STATUS_NORMAL = 1;
    const STATUS_LOCK = 2;
    const STATUS_INIT = 3;
}