Ext.define('app.pages.sys.role.AddEdit', {
    extend       : 'Ext.window.Window',
    width        : 400,
    height       : 190,
    modal        : true,
    resizable    : false,
    layout       : 'fit',
    title        : '添加角色',
    roleGrid     : null,
    roleRec      : null,
    requires     : ['app.comp.form.ClearTriggerPlugin', 'app.svc.sys.PermissionSvc'],
    initComponent: function () {
        var me = this;
        me.items = [
            {
                xtype   : 'form',
                name    : 'mainForm',
                padding : '10 10 0 10',
                layout  : {type: 'vbox', align: 'stretch'},
                defaults: {
                    margin          : '0 0 10 0',
                    labelWidth      : 70,
                    labelAlign      : 'right',
                    width           : '100%',
                    enforceMaxLength: true,
                    plugins         : ['ClearTriggerPlugin'],
                    getSubmitValue  : function () {
                        return Ext.String.trim(this.value);
                    }
                },
                items   : [
                    {
                        xtype              : 'textfield',
                        fieldLabel         : '角色名称',
                        name               : 'role_name',
                        maxLength          : 30,
                        cls                : 'field-required',
                        allowBlank         : false,
                        allowOnlyWhitespace: false
                    },
                    {
                        xtype     : 'textarea',
                        fieldLabel: '角色描述',
                        name      : 'role_desc',
                        maxLength : 60,
                        height    : 60
                    }
                ]
            }
        ]
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(arguments);
    },
    afterRender  : function () {
        var me = this;
        me.callParent(arguments);
        if (me.roleRec) {
            me.down('form').getForm().setValues(Ext.apply({}, me.roleRec.getData()))
        }
    },
    _saveClick   : function () {
        var me = this
        var form = me.down('form')
        if (!form.isValid()) {
            return;
        }
        var values = form.getValues();
        if (me.roleRec) {
            me._update(values)
        } else {
            me._add(values)
        }
    },
    _add         : function (params) {
        var me = this
        app.svc.sys.PermissionSvc.roleAdd.call(me,params).then(function (role) {
            if (me.roleGrid) {
                var rec = Ext.data.Model(role);
                me.roleGrid.getStore().insert(0, rec);
                me.roleGrid.setSelection(rec);
                me.roleGrid.getView().scrollTo(0, 0);
            }
            me.destroy()
        })
    },
    _update      : function (params) {
        var me = this
        params.sys_role_id = me.roleRec.get('sys_role_id')
        app.svc.sys.PermissionSvc.roleUpdate.call(me,params).then(function (role) {
            me.roleRec.set(role)
            me.roleRec.commit()
            me.destroy()
        })
    }
});