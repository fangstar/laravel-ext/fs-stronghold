/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

/**
 * 数据表store的抽象类
 * 如果需要使用model的话请使用columns属性，该属性即为model中的fields属性
 * @param {type} param1
 * @param {type} param2
 */
Ext.define('app.data.store.AbstractJsonReader', {
    extend: 'Ext.data.reader.Json',
    read  : function () {
        var self = this;
        var data = self.callParent(arguments);
        self.fireEvent('afterReadRecords', data);
        return data;
    },
    /**
     *
     * @param {type} excludeFilter 是否排除被过滤的数据
     * @returns {undefined}
     */
    getAllRawData: function (excludeFilter) {
        var self = this;
        var iterp = [];
        if (excludeFilter) {
            self.each(function (record) {
                iterp.push(record.raw);
            });
        } else {
            self.queryBy(function (record, id) {
                iterp.push(record.raw);
                return true;
            });
        }
        return iterp;
    },
    readRecords  : function (data, readOptions) {
        var me = this,
            meta;
        me.fireEvent('beforeReadRecords', data);
        if (me.getMeta) {
            meta = me.getMeta(data);
            if (meta) {
                me.onMetaChange(meta);
            }
        } else if (data.metaData) {
            me.onMetaChange(data.metaData);
        }
        return me.callParent([data, readOptions]);
    }

});