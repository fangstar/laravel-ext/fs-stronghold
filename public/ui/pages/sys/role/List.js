Ext.define('app.pages.sys.role.List', {
    extend        : 'Ext.grid.Panel',
    requires      : ['app.comp.column.OperationColumn', 'app.comp.column.Renderer', 'app.constant.Sys', 'app.util.Msg', 'app.svc.sys.PermissionSvc'],
    initComponent : function () {
        var me = this;
        me.tbar = [
            {
                xtype: 'form', layout: 'hbox', items: [
                    {
                        xtype: 'textfield', name: 'search', emptyText: '输入角色名称查询', listeners: {
                            change: function (view, newVal) {
                                me.getStore().smartSearch(newVal, ['role_name'])
                            }
                        }
                    },
                    {xtype: 'button', text: '重置', handler: me._reset, scope: me}
                ]
            },
            '-',
            {text: '添加角色', handler: me._add, scope: me}
        ];
        me.columns = [
            {text: '角色编号', dataIndex: 'sys_role_id'},
            {text: '角色名称', dataIndex: 'role_name'},
            {text: '角色描述', dataIndex: 'role_desc', width: 200},
            {text: '分配权限数', dataIndex: 'permission_cnt'},
            {text: '分配人数', dataIndex: 'user_cnt'},
            {text: '创建时间', dataIndex: 'created_at', width: 150, renderer: app.comp.column.Renderer.dateTimeRenderer},
            {text: '最后修改时间', dataIndex: 'updated_at', width: 150, renderer: app.comp.column.Renderer.dateTimeRenderer},
            {
                xtype: 'operationcolumn', minWidth: 200, flex: 1, items: [
                    {text: '编辑', handler: me._edit, scope: me},
                    {
                        text: '删除', handler: me._del, scope: me, isDisabled: function (view, rowIdx, colIdx, item, record) {
                            return (record.get('role_cate') == app.constant.Sys.role.cateSys)
                        }
                    },
                    {text: '分配权限', handler: me._setPermission, scope: me},
                    {text: '分配人员', handler: me._setUser, scope: me}
                ]
            }
        ];
        me.store = app.svc.sys.PermissionSvc.roleListStore()
        me.callParent(arguments);
    },
    _reset        : function () {
        var me = this
        me.down('textfield').setValue('')
        me.getStore().reload()
    },
    _add          : function () {
        var me = this
        me._addEdit({title: '添加角色', roleGrid: me});
    },
    _edit         : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this
        var roleName = record.get('role_name')
        me._addEdit({title: '编辑角色【' + roleName + '】', roleRec: record});
    },
    _del          : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this
        if (record.get('role_cate') == app.constant.Sys.role.cateSys) {
            app.util.Msg.error('系统角色不能删除')
            return
        }
        var roleName = record.get('role_name')
        app.util.Msg.confirm('删除确认', '确认要删除角色【' + roleName + '】吗？').then(function () {
            app.svc.sys.PermissionSvc.roleDel.call(me, record.get('sys_role_id')).then(function () {
                me.getStore().remove(record)
            })
        })
    },
    _setPermission: function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var win = Ext.create('app.pages.sys.role.Permission', {$record: record});
        win.getCallbackPromise().then(function (permission) {
            record.set(permission)
            record.commit()
        })
        win.show()
    },
    _setUser      : function () {
        var win = Ext.create('app.comp.EmpSelect')
        win.getCallbackPromise().then(function (empInfo) {

        })
        win.show();
    },
    _addEdit      : function (conf) {
        Ext.create('app.pages.sys.role.AddEdit', conf).show()
    }
})