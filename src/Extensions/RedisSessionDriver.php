<?php

namespace Fstar\Stronghold\Extensions;

use Illuminate\Support\Facades\Redis;

class RedisSessionDriver implements \SessionHandlerInterface {
    private $sessionRedis;
    private $sessionLifetime;
    private $session_prefix;
    private $renant_prefix;

    public function __construct() {
        $this->sessionRedis = Redis::connection('session');
        $this->sessionLifetime = config('session.lifetime') * 60;
        $this->session_prefix = 'session:data:';
        $this->renant_prefix = 'session:user:';
    }

    /**
     * @param string $save_path
     * @param string $session_name
     *
     * @return bool
     */
    public function open($save_path, $session_name) {
        return true;
    }

    /**
     * @return bool
     */
    public function close() {
        return true;
    }

    /**
     * @param string $session_id
     *
     * @return mixed|string
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function read($session_id) {
        $sessionDataJsonStr = $this->sessionRedis->get("{$this->session_prefix}{$session_id}");
        if(is_null($sessionDataJsonStr)) {
            return "";
        } else {
            $sessionDataArr = json_decode($sessionDataJsonStr, true);
            return serialize($sessionDataArr);
        }
    }

    /**
     * @param string $session_id
     * @param string $data
     *
     * @return bool|void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function write($session_id, $session_data) {
        $session_data_arr = unserialize($session_data);
        $session_key = "{$this->session_prefix}{$session_id}";
        $this->sessionRedis->set($session_key, json_encode($session_data_arr), 'EX', $this->sessionLifetime);

        $user_id = '';
        if(array_key_exists('sys_user_id', $session_data_arr)) {
            $user_id = $session_data_arr['sys_user_id'];
        }
        $renant_key = "{$this->renant_prefix}{$user_id}";
        $sid_list = $this->sessionRedis->hkeys($renant_key);
        if(!is_array($sid_list)) {
            $this->sessionRedis->del($renant_key);
            $sid_list = [];
        }
        if(!empty($user_id)) {
            $this->sessionRedis->hset($renant_key, $session_id, true);
        }
        if(array_key_exists('user_mult_login', $session_data_arr) && $session_data_arr['user_mult_login']) {
            foreach($sid_list as $sid) {
                if(!$this->sessionRedis->exists("{$this->session_prefix}{$sid}")) {
                    $this->sessionRedis->hdel($renant_key, $sid);
                }
            }
        } else {
            foreach($sid_list as $sid) {
                //踢会话
                if($sid != $session_id) {
                    $this->sessionRedis->hdel($renant_key, $sid);
                    $this->sessionRedis->del("{$this->session_prefix}{$sid}");
                }
            }
        }
        $this->sessionRedis->expire($renant_key, $this->sessionLifetime);
        $this->sessionRedis->expire($session_key, $this->sessionLifetime);
        return true;
    }

    public function tick($session_id) {
        $session_data = $this->read($session_id);
        $session_data_arr = unserialize($session_data);
        $user_id = '';
        if(array_key_exists('sys_user_id', $session_data_arr)) {
            $user_id = $session_data_arr['sys_user_id'];
        }
        $session_key = "{$this->session_prefix}{$session_id}";
        $renant_key = "{$this->renant_prefix}{$user_id}";
        $sid_list = $this->sessionRedis->hkeys($renant_key);
        if(!is_array($sid_list)) {
            $sid_list = [];
        }
        if(!empty($user_id)) {
            foreach($sid_list as $sid) {
                //踢会话
                if($sid != $session_id) {
                    $this->sessionRedis->hdel($renant_key, $sid);
                    $this->sessionRedis->del("{$this->session_prefix}{$sid}");
                }
            }
        }
        return $sid_list;
    }

    /**
     * @param string $session_id
     *
     * @return bool
     */
    public function destroy($session_id) {
        $sessionDataJsonStr = $this->sessionRedis->get("{$this->session_prefix}{$session_id}");
        if(!is_null($sessionDataJsonStr)) {
            $session_data_arr = json_decode($sessionDataJsonStr, true);
            if(array_key_exists('sys_user_id', $session_data_arr)) {
                $user_id = $session_data_arr['sys_user_id'];
                if(!empty($user_id)) {
                    $this->sessionRedis->hdel("{$this->renant_prefix}{$user_id}", $session_id);
                }
            }
        }
        $this->sessionRedis->del("{$this->session_prefix}{$session_id}");
        return true;
    }

    /**
     * @param int $lifetime
     *
     * @return bool
     */
    public function gc($lifetime) {
        return true;
    }

}
