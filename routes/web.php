<?php

use Illuminate\Support\Facades\Route;
use Fstar\Stronghold\Http\Controllers\AuthController;
use Fstar\Stronghold\Http\Controllers\PermissionController;
use Fstar\Stronghold\Http\Controllers\UserController;
use Fstar\Stronghold\Http\Middleware\SessionCheck;

Route::middleware('web')->prefix('fs')->group(function() {
    $session_check = config('fstar-stronghold.middleware.session_check');
    Route::prefix('auth')->group(function() {
        Route::any('login-by-pwd', [AuthController::class, 'loginByPwd']);
        Route::any('has-login', [AuthController::class, 'hasLogin']);
        Route::any('logout', [AuthController::class, 'logout']);
        Route::any('login-user', [AuthController::class, 'getLoginUser']);
    });

    Route::prefix('permission')->middleware($session_check)->group(function() {
        Route::any('role/list', [PermissionController::class, 'roleList']);
        Route::any('role/add', [PermissionController::class, 'roleAdd']);
        Route::any('role/update', [PermissionController::class, 'roleUpdate']);
        Route::any('role/del', [PermissionController::class, 'roleDel']);
        Route::any('role/permission', [PermissionController::class, 'getPermissionByRoleId']);
        Route::any('role/save-permission', [PermissionController::class, 'saveRolePermission']);

        Route::any('menus/manage', [PermissionController::class, 'getEmpManageMenus']);
        Route::any('menus/client', [PermissionController::class, 'getEmpClientMenus']);
    });

    Route::prefix('user')->middleware($session_check)->group(function() {
        Route::any('get-conf', [UserController::class, 'getUserConf']);
    });
});
