<?php

namespace Fstar\Stronghold\Models;

class SysRoleM extends BaseModel {
    protected $table = 'sys_role';
    protected $primaryKey = 'sys_role_id';
    protected $fillable = ['sys_role_id', 'role_cate', 'role_name', 'role_desc',
                           'delete_flag', 'create_id', 'update_id', 'delete_id', 'created_at', 'updated_at', 'deleted_at'];

    const CATE_SYS = 1;
    const CATE_USER = 2;
}