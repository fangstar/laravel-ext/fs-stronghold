Ext.define('app.pages.sys.task.List', {
    extend       : 'Ext.container.Container',
    requires     : ['app.comp.column.OperationColumn', 'app.comp.column.Renderer', 'app.constant.Sys', 'app.util.Msg', 'app.svc.sys.PermissionSvc'],
    layout       : 'border',
    initComponent: function () {
        var me = this;
        me.items = [
            {
                xtype      : 'gridpanel',
                region     : 'center',
                tbar       : [
                    {
                        xtype: 'form', flex: 1, defaults: {labelAlign: 'right'}, layout: 'hbox', items: [
                            {xtype: 'textfield', width: 240, fieldLabel: '任务名称', labelWidth: 60, name: 'timed_task_name'},
                            {xtype: 'combobox', width: 140, fieldLabel: '项目', labelWidth: 40, editable: false, name: 'timed_task_exec_proj'},
                            {xtype: 'textfield', width: 140, fieldLabel: '模块', labelWidth: 40, name: 'timed_task_exec_module'},
                            {xtype: 'combobox', width: 160, fieldLabel: '任务状态', labelWidth: 70, editable: false, name: 'timed_task_status'},
                            {xtype: 'combobox', width: 160, fieldLabel: '执行状态', labelWidth: 70, editable: false, name: 'timed_task_exec_status'},
                            {xtype: 'datefield', width: 180, fieldLabel: '执行时间', labelWidth: 70, editable: false, name: 'timed_task_start_at'},
                            {xtype: 'displayfield', value: '至'},
                            {xtype: 'datefield', width: 120, labelWidth: 25, editable: false, name: 'timed_task_end_at'},
                            {xtype: 'textfield', width: 180, fieldLabel: 'ID', labelWidth: 30, name: 'timed_task_def_id', emptyText: '多个ID用,分隔'},
                            {xtype: 'button', text: '查询'},
                            {xtype: 'button', text: '重置'}
                        ]
                    }
                ],
                columns    : [
                    {text: 'ID', width: 40, dataIndex: 'timed_task_def_id', sortable: true},
                    {text: 'KEY', width: 150, hidden: true, dataIndex: 'timed_task_key'},
                    {text: '名称', width: 150, dataIndex: 'timed_task_name', sortable: true},
                    {text: '项目', width: 60, dataIndex: 'timed_task_exec_proj', sortable: true},
                    {text: '模块', width: 60, dataIndex: 'timed_task_exec_module', sortable: true},
                    {text: '任务状态', width: 80, dataIndex: 'timed_task_status', sortable: true},
                    {text: '执行状态', width: 80, dataIndex: 'timed_task_exec_status', sortable: true},
                    {text: 'class', width: 250, dataIndex: 'timed_task_class', sortable: true},
                    {text: '执行method', width: 100, dataIndex: 'timed_task_method', sortable: true},
                    {text: '执行命令', hidden: true, width: 100, dataIndex: 'timed_task_command', sortable: true},
                    {text: '执行频率', width: 150, dataIndex: 'timed_task_frequency', sortable: true},
                    {text: '时间参数', width: 80, dataIndex: 'timed_task_params', sortable: true},
                    {text: '执行参数', width: 80, dataIndex: 'timed_task_exec_params'},
                    {text: '超时时间(秒)', width: 100, align: 'center', dataIndex: 'timed_task_exec_timeout', sortable: true},
                    {text: '上次开始时间', width: 120, dataIndex: 'timed_task_start_at', sortable: true},
                    {text: '上次结束时间', width: 120, dataIndex: 'timed_task_end_at', sortable: true},
                    {text: '上次花费(秒)', width: 115, align: 'center', dataIndex: 'timed_task_cast', sortable: true},
                    {text: '任务描述', minWidth: 100, flex: 1, dataIndex: 'timed_task_desc'},
                    {
                        text     : '错误信息', width: 100,
                        flex     : 2,
                        dataIndex: 'timed_task_exec_err_msg',
                        renderer : function (v, cellValues, record, rowIdx, colIdx, store, view) {
                            return Ext.String.htmlEncode(v);
                        }
                    }
                ],
                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    // store: 'simpsonsStore', // same store GridPanel is using
                    dock       : 'bottom',
                    displayInfo: true
                }]
            },
            {
                xtype: 'container', layout: 'border', split: true, region: 'south', height: '50%', items: [
                    {
                        xtype: 'gridpanel', region: 'center', columns: [
                            {text: '任务ID', width: 60, dataIndex: 'task_id'},
                            {text: '记录ID', width: 220, hidden: true, dataIndex: 'id'},
                            {text: '状态', width: 60, dataIndex: 'exec_status', sortable: true},
                            {text: '执行开始时间', width: 120, dataIndex: 'start_at', sortable: true},
                            {text: '执行结束时间', width: 120, dataIndex: 'end_at', sortable: true},
                            {text: '执行时长(秒)', width: 100, align: 'center', dataIndex: 'exec_cast', sortable: true},
                            {text: '执行人', width: 70, dataIndex: 'exec_emp_name', sortable: true},
                            {text: '执行部门', width: 80, dataIndex: 'exec_dept_name', sortable: true},
                            {
                                text    : '数量(成功/可执行/总数)',
                                width   : 160,
                                renderer: function (v, cellValues, record, rowIdx, colIdx, store, view) {
                                    var find_task_cnt    = record.get('find_task_cnt'),
                                        exec_task_cnt    = record.get('exec_task_cnt'),
                                        exec_success_cnt = record.get('exec_success_cnt');
                                    if (find_task_cnt !== undefined && find_task_cnt > 0) {
                                        return Ext.String.format('{0} / {1} / {2}', exec_success_cnt, exec_task_cnt, find_task_cnt);
                                    }
                                }
                            },
                            {text: '错误信息', flex: 1, dataIndex: 'error_msg', renderer: Ext.String.htmlEncode}
                        ]
                    },
                    {
                        xtype: 'gridpanel', region: 'east', split: true, width: '50%', columns: []
                    }
                ]
            }
        ];
        me.callParent(arguments);
    }
})