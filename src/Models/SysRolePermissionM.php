<?php

namespace Fstar\Stronghold\Models;

class SysRolePermissionM extends BaseModel {
    protected $table = 'sys_role_permission';
    protected $primaryKey = 'sys_role_permission_id';
}