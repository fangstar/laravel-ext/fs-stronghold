<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_route', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('路由表');
            $table->id('sys_route_id')->comment('路由ID');
            $table->tinyInteger('route_type')->default(1)->comment('路由类型  1：用户端   2：管理端');
            $table->string('route_name', 31)->nullable()->comment('名称');
            $table->string('route_path', 127)->comment('地址');
            $table->tinyInteger('route_status')->default(1)->comment('路由状态 1：启用 2：停用');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
