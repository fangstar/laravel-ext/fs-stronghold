<?php

namespace Fstar\Stronghold\Services;

use Fstar\Stronghold\Constants\FsConstant;
use Fstar\Stronghold\Exception\AccountNeedInitException;
use Fstar\Stronghold\Exception\FsStrongholdException;
use Fstar\Stronghold\Exception\NotFoundException;
use Fstar\Stronghold\Models\SysPermissionM;
use Fstar\Stronghold\Models\SysUserM;
use Fstar\Stronghold\Models\SysRoleM;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class AuthService {
    public function loginByPwd($user_account, $user_pwd) {
        $userM = SysUserM::where('user_account', $user_account)->deleteNo()->first();
        if($userM == null) {
            throw new NotFoundException("用户不存在");
        }
        if($userM->user_status == SysUserM::STATUS_INIT) {
            throw new AccountNeedInitException("密码需要初始化");
        }
        if($userM->user_status != SysUserM::STATUS_NORMAL) {
            throw new FsStrongholdException("用户账户状态错误，无法登陆");
        }
        if(!Hash::check($user_pwd, $userM->user_pwd)) {
            throw new FsStrongholdException("用户或密码错误");
        }
        $user = $this->getSessionInfo($userM->sys_user_id);
        session($user);
        return $user;
    }

    public function getLoginUser() {
        $permission_key              = [];
        $emp_id                      = session('emp_id');
        $emp_info                    = session()->all();
        $emp_info['permission_keys'] = SysRoleM::join('sys_user_role as ur', 'ur.sys_role_id', '=', 'sys_role.sys_role_id')
                                               ->join('sys_role_permission as rp', 'rp.sys_role_id', '=', 'ur.sys_role_id')
                                               ->join('sys_permission as p', 'p.sys_permission_id', '=', 'rp.sys_permission_id')
                                               ->where('ur.sys_user_id', $emp_id)
                                               ->where('ur.delete_flag', FsConstant::DEL_NO)
                                               ->where('rp.delete_flag', FsConstant::DEL_NO)
                                               ->where('sys_role.delete_flag', FsConstant::DEL_NO)
                                               ->where('p.permission_status', SysPermissionM::STATUS_ENABLE)
                                               ->get(['p.sys_permission_key', 'p.sys_permission_name'])
                                               ->pluck('sys_permission_name', 'sys_permission_key')
                                               ->toArray();
        return $emp_info;
    }

    public function getSessionInfo($user_id) {
        $empM = SysUserM::join('hr_emp as e', 'e.emp_id', '=', 'sys_user.sys_user_id')
                        ->join('sys_tenant as t', 'e.sys_tenant_id', '=', 't.sys_tenant_id')
                        ->where('sys_user.sys_user_id', $user_id)
                        ->first(['sys_user.sys_user_id', 'user_account', 'user_mult_login', 'user_ui_style', 'e.*', 't.*']);
        if($empM == null) {
            throw new NotFoundException('找不到用户信息，检查hr_emp是否关联了用户');
        }
        $user             = Arr::except($empM->toArray(), ['tenant_sk', 'tenant_no', 'biz_db_name', 'delete_flag', 'created_at', 'updated_at', 'deleted_at', 'create_id', 'update_id', 'delete_id']);
        $user['is_login'] = true;
        return $user;
    }
}
