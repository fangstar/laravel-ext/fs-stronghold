Ext.define('app.constant.TopicEvent', {
    statics: {
        SYS: {
            APP_DESKTOP_CREATE_WIN: 'APP_DESKTOP_CREATE_WIN',
            APP_TAB_OPEN_TAB      : 'APP_TAB_OPEN_TAB'
        }

    }
})