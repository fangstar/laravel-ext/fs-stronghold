Ext.define('app.viewport.tabComp.NaviTree', {
    extend       : 'Ext.tree.Panel',
    alias        : ['widget.tabnavitree'],
    requires     : ['app.svc.sys.PermissionSvc', 'app.constant.TopicEvent', 'app.comp.Topic'],
    rootVisible  : false,
    displayField : 'node_name',
    initComponent: function () {
        var me = this;
        me.listeners = {
            itemclick: me._onItemClick,
            scope    : me
        }
        me.callParent(arguments);
    },
    getMenus     : function () {
        var me = this
        var menuMap = {
            0: {node_id: 0, node_name: 'APP', leaf: false, children: []}
        }
        var firstNode = null
        app.svc.sys.PermissionSvc.getMenus.call(me).then(function (menus) {
            Ext.Array.each(menus, function (item) {
                item.leaf = true
                item.iconCls = item.node_icon_cls;
                Ext.apply(item, {
                    leaf     : true,
                    iconCls  : item.node_icon_cls,
                    node_id  : item.sys_node_id,
                    node_conf: Ext.decode(item.node_conf, true) || {}
                });
                if (!item.node_is_leaf) {
                    item.children = [];
                    item.leaf = false;
                } else if (!firstNode) {
                    firstNode = item
                }
                menuMap[item.node_id] = item;
            })
            Ext.Array.each(menus, function (item) {
                if (menuMap[item.node_pid] && Ext.isArray(menuMap[item.node_pid].children)) {
                    menuMap[item.node_pid].children.push(item);
                }
            });
            if (menuMap[0].children[0]) {
                menuMap[0].children[0].expanded = true
            }
            me.setStore(Ext.create('Ext.data.TreeStore', {
                root: menuMap[0]
            }));
            if (firstNode) {
                app.comp.Topic.publish(app.constant.TopicEvent.SYS.APP_TAB_OPEN_TAB, firstNode)
            }
        });
    },
    afterRender  : function () {
        var me = this
        me.callParent(arguments);
        me.getMenus();
    },
    _onItemClick : function (treePanel, node, item, index, e, eOpts) {
        var me = this;
        //不是叶子节点,或未配置路由,不进行任何操作.
        if (!node.get('leaf') || Ext.isEmpty(node.get('node_route_path'))) {
            return;
        }
        app.comp.Topic.publish(app.constant.TopicEvent.SYS.APP_TAB_OPEN_TAB, node.getData())
    }
})