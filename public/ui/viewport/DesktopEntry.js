Ext.define('app.viewport.DesktopEntry', {
    extend          : 'Ext.ux.desktop.App',
    requires        : ['app.viewport.desktop.DesktopStartMenu', 'app.comp.Topic', 'app.constant.TopicEvent', 'app.svc.sys.AuthSvc',
                       'app.util.Msg', 'app.comp.AuthComp', 'app.comp.ConfComp', 'app.constant.Sys', 'app.svc.sys.PermissionSvc'],
    modules         : [],
    init            : function () {
        var me = this;
        me.queryModules().then(function (modules) {
            me.modules = modules;
            if (me.modules) {
                me.initModules(me.modules);
            }
            desktopCfg = me.getDesktopConfig();
            me.desktop = new Ext.ux.desktop.Desktop(desktopCfg);
            me.viewport = new Ext.container.Viewport({layout: 'fit', items: [me.desktop]});
            Ext.getWin().on('beforeunload', me.onUnload, me);
            me.isReady = true;
            me.fireEvent('ready', me);
            app.comp.AppLoading.hide();
        })
        app.comp.Topic.subscribe(app.constant.TopicEvent.SYS.APP_DESKTOP_CREATE_WIN, me._createWindow)
    },
    queryModules    : function () {
        var me = this;
        var deferred = new Ext.Deferred();
        app.comp.ConfComp.getUserConfByKey.call(me, app.constant.Sys.conf.user.desktopShortcut).then(function (shortcutConf) {
            shortcutConf = Ext.decode(shortcutConf, true) || []
            shortcutConf = Ext.isArray(shortcutConf) ? shortcutConf : [];
            var shortcutNodes = [];
            app.svc.sys.PermissionSvc.getMenus.call(me).then(function (menus) {
                var modules = [];
                var menuMap = {};
                Ext.Array.each(menus, function (item) {
                    if (item.node_pid == app.constant.Sys.rootNodeId) {
                        item.items = [];
                    }
                    item.node_conf = Ext.decode(item.node_conf, true) || {}
                    menuMap[item.sys_node_id] = item;
                    if (item.node_is_leaf && !Ext.isEmpty(item.node_route_path) && shortcutConf.includes(item.sys_node_id)) {
                        shortcutNodes.push(item)
                    }
                })
                Ext.Array.each(menus, function (item) {
                    if (menuMap[item.node_pid] && Ext.isArray(menuMap[item.node_pid].items)) {
                        menuMap[item.node_pid].items.push(item)
                    }
                })
                var hasShortcut = shortcutNodes.length > 0;
                for (var node_id in menuMap) {
                    var node = menuMap[node_id]
                    if (node.node_pid == app.constant.Sys.rootNodeId) {
                        modules.push(Ext.create('app.viewport.desktop.DesktopStartMenu', {node: node}))
                    } else if (!hasShortcut && node.node_is_leaf && !Ext.isEmpty(node.node_route_path) && node.node_conf.shortcut) {
                        shortcutNodes.push(node)
                    }
                }
                me._shortcuts = shortcutNodes
                deferred.resolve(modules)
            })
        })
        return deferred.promise;
    },
    getDesktopConfig: function () {
        var me = this
        var ret = me.callParent();

        return Ext.apply(ret, {
            contextMenuItems   : [],
            shortcutTpl        : [
                '<tpl for=".">',
                '<div class="ux-desktop-shortcut" id="{node_id}-shortcut">',
                '<div class="ux-desktop-shortcut-icon {node_icon_cls}">',
                '<img src="{node_icon_url}" title="{node_name}">',
                '</div>',
                '<span class="ux-desktop-shortcut-text">{node_name}</span>',
                '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
            ],
            shortcuts          : Ext.create('Ext.data.Store', {
                data: me._shortcuts
            }),
            wallpaper          : '/img/Wood-Sencha.jpg',
            wallpaperStretch   : true,
            onShortcutItemClick: function (dataView, record) {
                var me = this;
                var node = record.data
                var desktop = me.app.getDesktop();
                app.comp.Topic.publish(app.constant.TopicEvent.SYS.APP_DESKTOP_CREATE_WIN, {desktop: desktop, node: node});
            }
        });
    },

    // config for the start menu
    getStartConfig: function () {
        var me = this, ret = me.callParent();
        return Ext.apply(ret, {
            title     : '开始菜单',
            iconCls   : 'user',
            minHeight : 300,
            toolConfig: {
                width: 100,
                items: [
                    {
                        text   : '设置',
                        iconCls: 'settings',
                        handler: me.onSettings,
                        scope  : me
                    },
                    '-',
                    {
                        text   : '退出',
                        iconCls: 'logout',
                        handler: me.onLogout,
                        scope  : me
                    }
                ]
            }
        });
    },

    getTaskbarConfig: function () {
        var ret = this.callParent();
        return Ext.apply(ret, {
            startBtnText: '开始菜单',
            trayItems   : [
                {xtype: 'trayclock', flex: 1}
            ]
        });
    },

    _createWindow: function (param) {
        var desktop = param.desktop
        var node = param.node;
        var winId = 'fs_window_' + node.node_id;
        var win = desktop.getWindow(winId);
        var nodeConf = Ext.isString(node.node_conf) ? (Ext.decode(node.node_conf, true) || {}) : node.node_conf;
        if (!win) {
            win = desktop.createWindow({
                                           id             : winId,
                                           title          : node.node_name,
                                           width          : nodeConf.width || 640,
                                           height         : nodeConf.height || 480,
                                           iconCls        : node.node_icon_cls,
                                           maximized      : nodeConf.maximized || false,
                                           animCollapse   : false,
                                           constrainHeader: true,
                                           layout         : 'fit',
                                           items          : [Ext.create(node.node_route_path)]
                                       });
        }
        if (win) {
            desktop.restoreWindow(win);
        }
    },
    onLogout     : function () {
        app.comp.AuthComp.logout();
    },

    onSettings: function () {

    }
})