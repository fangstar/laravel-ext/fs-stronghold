/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */
Ext.define('app.comp.column.TipColumn', {
    extend         : 'Ext.grid.column.Column',
    alias          : ['widget.tipcolumn'],
    tipType        : 'title',
    emptyText      : '',
    defaultRenderer: function (v, cellValues, record, rowIdx, colIdx, store, view) {
        var tip = v = Ext.isEmpty(v) ? this.emptyText : Ext.String.htmlEncode(v);
        v = '<span data-qtip="' + tip + '">' + v + '</span>';
        return v;
    }
});
