Ext.define('app.comp.mixin.CompDeferred', {
    getCallbackPromise: function () {
        var me = this
        me.deferred = new Ext.Deferred()
        return me.deferred
    }
})