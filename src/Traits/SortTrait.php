<?php

namespace Fstar\Stronghold\Traits;

use Exception;

trait SortTrait {

    /**
     * 处理排序参数
     *
     * @param String $field     默认排序字段
     * @param String $direction 默认排序方向
     *
     * @return array
     * @throws Exception
     */
    protected function getSortParams(string $field, string $direction = 'desc', array $sort_conf = []): array {
        $sort = request('sort', null);
        if(!is_null($sort)) {
            try {
                $sortArry = json_decode($sort, true);
                if(is_array($sortArry) && count($sortArry) > 0) {
                    if(!empty($sortArry['property'])) {
                        $field = $sortArry['property'];
                        if(!empty($sortArry['direction'])) {
                            $direction = $sortArry['direction'];
                        }
                    } else {
                        $field = $sortArry[0]['property'];
                        if(!empty($sortArry[0]['direction'])) {
                            $direction = $sortArry[0]['direction'];
                        }
                    }
                }
            } catch(\Exception $ex) {
                report($ex);
            }
        }
        return ['field' => data_get($sort_conf, $field, $field), 'direction' => $direction];
    }

}
