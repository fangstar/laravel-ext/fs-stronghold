<?php

namespace Fstar\Stronghold\Http\Controllers;

use Fstar\Stronghold\Traits\ResponseTrait;
use Illuminate\Routing\Controller;

class BaseController extends Controller {
    use ResponseTrait;
}