<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_permission', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('系统权限表');
            $table->id('sys_permission_id')->comment('权限ID');
            $table->integer('sys_permission_group_id')->comment('权限分组ID');
            $table->string('sys_permission_key', 63)->unique('idx_sys_permission_key_unique')->comment('权限KEY');
            $table->string('sys_permission_name', 31)->nullable()->comment('权限名称');
            $table->tinyInteger('permission_status')->default(1)->comment('权限状态 1：启用  2：停用');
            $table->string('permission_desc', 63)->nullable()->comment('权限描述');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
