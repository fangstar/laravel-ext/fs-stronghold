Ext.define('app.viewport.TabEntry', {
    extend       : 'Ext.container.Container',
    requires     : ['app.comp.Topic', 'app.constant.TopicEvent', 'app.comp.AppLoading', 'app.viewport.tabComp.HeaderBar', 'app.viewport.tabComp.NaviTree'],
    layout       : 'border',
    initComponent: function () {
        var me = this;
        me.items = [
            {
                xtype : 'tabheaderbar',
                region: 'north'
            },
            {
                xtype : 'tabnavitree',
                region: 'west',
                split : true,
                width : 300
            },
            {
                xtype     : 'tabpanel',
                region    : 'center',
                scrollable: true,
                plugins   : ['tabclosemenu', 'tabreorderer', 'tabscrollermenu']
            }
        ];
        me.callParent(arguments);
    },
    afterRender  : function () {
        var me = this;
        app.comp.AppLoading.hide();
        app.comp.Topic.subscribe(app.constant.TopicEvent.SYS.APP_TAB_OPEN_TAB, me._openTab, me);
        me.callParent(arguments);
    },
    _openTab     : function (node) {
        var me = this;
        var tabpanel = me.down('tabpanel');
        var moduleId = 'page_module_' + node.node_route_path.replace(new RegExp('\\.', 'g'), '_')
        var tab = tabpanel.child('#' + moduleId);
        if (!tab) {
            try {
                tab = Ext.create(node.node_route_path, {
                    id      : moduleId,
                    title   : node.node_name,
                    closable: node.node_conf.closable === false ? false : true,
                    iconCls : node.node_icon_cls
                });
                tabpanel.add(tab);
            } catch (err) {
                console.error(err);
            }
        }
        tabpanel.setActiveItem(tab);
    }
})