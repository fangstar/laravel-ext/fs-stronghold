<?php

namespace Fstar\Stronghold\Traits;

use App\Constants\ErrCode;
use Exception;

trait PageTrait {

    /**
     * 处理分页参数
     *
     * @param bool $limit_max_size 是否需要限制最大分页数量
     *
     * @return array
     * @throws Exception
     */
    protected function getPageParams(bool $limit_max_size = true): array {

        $validator = validator(request()->all(),
                               [
                                   'page'     => 'required|numeric',
                                   'pagesize' => 'required|numeric',
                               ]);
        if($validator->fails()) {
            throw new \Exception($validator->errors()->first(), ErrCode::INFO_ERROR_PARAM);
        }
        $page = request('page');
        $pagesize = request('pagesize');
        if($limit_max_size) {
            $max_page_size = config('env.page_size.mgr_max_size');
            if($pagesize > $max_page_size) {
                $pagesize = $max_page_size;
            }
        }
        $skip = $pagesize * ($page - 1);
        return ['page' => $page, 'skip' => $skip, 'pagesize' => $pagesize, 'limit' => $pagesize];
    }

    protected function getPageParamsUseDefault(bool $limit_max_size = true): array {

        $max_page_size = config('env.page_size.mgr_max_size');
        $page = request('page', 1);
        $pagesize = request('pagesize', $max_page_size);
        if($limit_max_size) {
            if($pagesize > $max_page_size) {
                $pagesize = $max_page_size;
            }
        }
        $skip = $pagesize * ($page - 1);
        return ['page' => $page, 'skip' => $skip, 'pagesize' => $pagesize, 'limit' => $pagesize];
    }

}