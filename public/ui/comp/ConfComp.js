Ext.define('app.comp.ConfComp', function () {
    var userConf = null;
    return {
        requires : ['app.svc.sys.AuthSvc', 'app.util.Msg', 'app.svc.sys.UserSvc'],
        statics  : {
            getUserConf     : function () {
                var me = this
                var deferred = new Ext.Deferred();
                if (userConf) {
                    deferred.resolve(userConf);
                } else {
                    app.svc.sys.UserSvc.getUserConf.call(me).then(function (conf) {
                        userConf = conf || {}
                        deferred.resolve(userConf[key]);
                    });
                }
                return deferred.promise;
            },
            getUserConfByKey: function (key) {
                var me = this
                var deferred = new Ext.Deferred();
                if (userConf) {
                    deferred.resolve(userConf[key]);
                } else {
                    app.svc.sys.UserSvc.getUserConf.call(me).then(function (conf) {
                        userConf = conf || {}
                        deferred.resolve(userConf[key]);
                    });
                }
                return deferred.promise;
            }
        }
    }
})