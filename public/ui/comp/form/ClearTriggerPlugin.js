/**
 * 清除插件.
 */
Ext.define('app.comp.form.ClearTriggerPlugin', {
    extend  : 'Ext.AbstractPlugin',
    alias   : ['plugin.cleartriggerplugin', 'plugin.ClearTriggerPlugin'],
    requires: [],

    init: function (component) {
        if (!component.getValue) {
            return;
        }

        this.component = component;
        if (!component.rendered) {
            component.on('afterrender', this.handleAfterRender, this);
        } else {
            this.handleAfterRender();
        }
    },

    handleAfterRender: function () {
        var component = this.component;
        // if (component.readOnly || component.disabled) {
        //     return;
        // }
        if (!component.inputWrap) {
            return;
        }

        var el = Ext.fly(this.component.el);
        var clearBtn = this.clearBtn = component.inputWrap.insertSibling({
                                                                             tag  : 'div',
                                                                             cls  : 'x-form-trigger x-form-trigger-default x-form-clear-trigger x-form-clear-trigger-default',
                                                                             style: {
                                                                                 display: 'none'
                                                                             }
                                                                         }, 'after');

        Ext.create('Ext.tip.ToolTip', {
            target: clearBtn,
            html  : '清除'
        });

        clearBtn.setVisibilityMode(Ext.dom.Element.DISPLAY);

        clearBtn.on('click', this.clearValue, this);
        //component.on('change',this.update,this);

        if (el && el.on) {
            el.on('mouseover', function () {
                if (this.hasValue() && !component.disabled && !component.readOnly) {
                    this.show();
                } else {
                    this.hide();
                }
            }, this);
            el.on('mouseout', this.hide, this);
        }
    },

    hasValue: function () {
        var value = this.component.getValue();
        if (value === undefined || value === null || value === '') {
            return false;
        }
        return true;
    },

    clearValue: function () {
        if (this.component.xtype == "monthfield") {
            this.component.reset()
        } else {
            this.component.setValue(null);
        }
        this.hide();
    },

    hide: function () {
        this.clearBtn.hide();
    },
    show: function () {
        if (this.clearBtn.next('.x-form-clear-trigger')) {
            return;
        }

        this.clearBtn.show();
    }

});