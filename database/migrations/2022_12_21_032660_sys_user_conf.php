<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_user_conf', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('用户配置信息表');
            $table->id('sys_user_conf_id')->autoIncrement()->comment('用户ID');
            $table->integer('sys_user_id')->index('idx_user_id')->comment('用户ID');
            $table->text('user_desktop_shortcut')->nullable()->comment('用户快捷键配置 node_id 数组');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->unique(['sys_user_id'], 'idx_sys_user_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
