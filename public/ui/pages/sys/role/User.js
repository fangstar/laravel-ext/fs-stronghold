Ext.define('app.pages.sys.role.User', {
    extend: 'Ext.window.Window'

})

/**
 * 分配角色人员.
 */
Ext.define('app.pages.sys.role.User', {
    extend       : 'Ext.window.Window',
    width        : 1000,
    height       : 540,
    minWidth     : 800,
    minHeight    : 540,
    title        : '角色分配人员',
    maximizable  : true,
    $record      : null,
    initComponent: function () {
        var self = this;
        self.items = [
            {
                xtype             : 'empmultiseltcmp',
                name              : 'mainCt',
                $seltedQueryUrl   : fangtu.Server.role.queryRoleEmp,
                $seltedQueryParams: {
                    role_id: self.$record ? self.$record.get('role_id') : null
                }
            }
        ];
        self.callParent(arguments);
    },
    $buttons     : [
        {text: '保存', handler: '_save'}
    ],
    _save        : function () {
        var self = this;
        if (!self.$record) {
            return fangtu.AppData.error('缺少角色记录');
        }
        var mainCt = self.down('[name=mainCt]');
        var ret = mainCt._getResult();
        if (ret === false) {
            return;
        }
        var role_emp_cnt = ret.emp_cnt;
        delete ret.emp_cnt;
        ret.role_id = self.$record.get('role_id');
        fangtu.AppData.ajaxReq(
            {
                mask        : [self],
                url         : fangtu.Server.role.setRoleEmp,
                params      : ret,
                succCallBack: function () {
                    fangtu.AppData.succToast();
                    self.$record.set({role_emp_cnt: role_emp_cnt});
                    self.$record.commit();
                    self.destroy();
                }
            }
        );
    }
});
