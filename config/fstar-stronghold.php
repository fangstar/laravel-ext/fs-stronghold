<?php

return [
    'resp'       => [
        'field' => [
            'success'   => 'success',
            'errorcode' => 'errorcode',
            'msg'       => 'msg',
            'data'      => 'data'
        ],
        'val'   => [
            'success'   => true,
            'error'     => false,
            'errorcode' => 0
        ]
    ],
    'err_code'   => [
        'no_login' => 100
    ],
    'middleware' => [
        'session_check' => 'session_check'
    ]
];
