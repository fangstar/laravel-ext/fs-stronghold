Ext.define('app.comp.AuthComp', function () {
    var empInfo = {};
    return {
        requires : ['app.svc.sys.AuthSvc', 'app.util.Msg', 'app.svc.sys.AuthSvc'],
        statics  : {
            queryLoginEmpInfo: function () {
                let me = this;
                return new Ext.Promise(function (resolve, reject) {
                    app.svc.sys.AuthSvc.queryLoginEmp.call(me, true).then(function (_empInfo) {
                        empInfo = _empInfo
                        resolve(_empInfo)
                    }).catch(function (err) {
                        reject(err)
                    })
                })
            },
            getEmpInfo       : function () {
                return empInfo;
            },
            hasPermission    : function (permissionKey) {
                return true
            },
            logout           : function () {
                var me = this
                app.util.Msg.confirm('退出登录确认', '确认要退出登录吗？').then(function (text) {
                    app.svc.sys.AuthSvc.logout.call(me).then(function () {
                        window.location = '/login.html'
                    });
                })
            }
        }
    }
})