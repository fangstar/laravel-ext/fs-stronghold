<?php

namespace Fstar\Stronghold\Http\Controllers;

use Fstar\Stronghold\Models\SysNodeM;
use Fstar\Stronghold\Services\PermissionService;

class PermissionController extends BaseController {
    public function getEmpManageMenus(PermissionService $svc) {
        $user_id = session('sys_user_id');
        $data = $svc->getEmpMenus($user_id, SysNodeM::CATE_MANAGE);
        return $this->respSuccessData($data);
    }

    public function getEmpClientMenus(PermissionService $svc) {
        $user_id = session('sys_user_id');
        $data = $svc->getEmpMenus($user_id, SysNodeM::CATE_CLIENT);
        return $this->respSuccessData($data);
    }

    public function roleList(PermissionService $svc) {
        $data = $svc->roleList();
        return $this->respSuccessData($data);
    }

    public function roleAdd(PermissionService $svc) {
        $params = request()->all();
        $validator = validator($params,
                               [
                                   'role_name' => 'required'
                               ],
                               [
                                   'required' => '缺少:attribute'
                               ],
                               [
                                   'role_name' => '角色名称'
                               ]);
        if($validator->fails()) {
            return $this->respError($validator->errors()->first());
        }
        $user_id = session('sys_user_id');
        $data = $svc->roleAdd($params, $user_id);
        return $this->respSuccessData($data);
    }

    public function roleUpdate(PermissionService $svc) {
        $params = request()->all();
        $validator = validator($params,
                               [
                                   'sys_role_id' => 'required'
                               ],
                               [
                                   'required' => '缺少:attribute'
                               ],
                               [
                                   'sys_role_id' => '角色ID'
                               ]);
        if($validator->fails()) {
            return $this->respError($validator->errors()->first());
        }
        $user_id = session('sys_user_id');
        $data = $svc->roleUpdate($params, $user_id);
        return $this->respSuccessData($data);
    }

    public function roleDel(PermissionService $svc) {
        $params = request()->all();
        $validator = validator($params,
                               [
                                   'sys_role_id' => 'required'
                               ],
                               [
                                   'required' => '缺少:attribute'
                               ],
                               [
                                   'sys_role_id' => '角色ID'
                               ]);
        if($validator->fails()) {
            return $this->respError($validator->errors()->first());
        }
        $user_id = session('sys_user_id');
        $data = $svc->roleDel($params['sys_role_id'], $user_id);
        return $this->respSuccessData($data);
    }

    public function getPermissionByRoleId(PermissionService $svc) {
        $params = request()->all();
        $validator = validator($params,
                               [
                                   'sys_role_id' => 'required'
                               ],
                               [
                                   'required' => '缺少:attribute'
                               ],
                               [
                                   'sys_role_id' => '角色ID'
                               ]);
        if($validator->fails()) {
            return $this->respError($validator->errors()->first());
        }
        $data = $svc->getPermissionByRoleId($params['sys_role_id']);
        return $this->respSuccessData($data);
    }

    public function saveRolePermission(PermissionService $svc) {
        $params = request()->all();
        $validator = validator($params,
                               [
                                   'sys_role_id' => 'required',
                               ],
                               [
                                   'required' => '缺少:attribute'
                               ],
                               [
                                   'sys_role_id' => '角色ID',
                               ]);
        if($validator->fails()) {
            return $this->respError($validator->errors()->first());
        }
        $user_id = session('sys_user_id');
        $data = $svc->saveRolePermission($params['sys_role_id'], data_get($params, 'add', []), data_get($params, 'upd', []), data_get($params, 'del', []), $user_id);
        return $this->respSuccessData($data);
    }
}