<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_user', function(Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->comment('用户信息表');
            $table->id('sys_user_id')->autoIncrement()->comment('用户ID');
            $table->string('user_account', 31)->comment('用户账号');
            $table->string('user_pwd', 127)->comment('用户密码');
            $table->tinyInteger('user_status')->default(1)->comment('用户状态 1:正常 2：锁定 3：重置密码');
            $table->tinyInteger('user_mult_login')->default(1)->comment('允许多点登录 1：是 0：否');
            $table->string('user_ui_style')->default('desktop')->comment('UI样式 desktop tab');
            $table->integer('user_del_rel_id')->default(0)->comment('删除辅助字段');
            $table->tinyInteger('delete_flag')->default(0)->comment('删除状态 0:正常  1:软删  2:删除');
            $table->bigInteger('created_at')->comment('创建时间');
            $table->bigInteger('updated_at')->nullable()->comment('更新时间');
            $table->bigInteger('deleted_at')->nullable()->comment('删除时间');
            $table->integer('create_id')->comment('创建人');
            $table->integer('update_id')->nullable()->comment('更新人');
            $table->integer('delete_id')->nullable()->comment('删除人');
            $table->unique(['user_account', 'user_del_rel_id'], 'idx_user_account_unique');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
};
