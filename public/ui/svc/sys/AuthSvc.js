Ext.define('app.svc.sys.AuthSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        loginByPwd   : function (user_account, user_pwd) {
            return app.svc.BaseSvc.ajax.call(this,{url: '/fs/auth/login-by-pwd', params: {user_account: user_account, user_pwd: user_pwd}});
        },
        logout       : function () {
            return app.svc.BaseSvc.ajax.call(this,{url: '/fs/auth/logout'});
        },
        hasLogin     : function () {
            return app.svc.BaseSvc.ajax.call(this,{url: '/fs/auth/has-login'});
        },
        queryLoginEmp: function (hideErrMsg) {
            return app.svc.BaseSvc.ajax.call(this,{url: '/fs/auth/login-user', hideMask: true, hideErrMsg: hideErrMsg});
        }
    }
})